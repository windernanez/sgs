<?php
	include("includes/conexion.php");	
?>
<div class="cajas">
	<div class="titulos_cajas">Autoridades Registradas</div>

					
		<?php
		$datos = $mysqli->query("SELECT * FROM autoridades ORDER BY nombre ASC");	

		if($datos->num_rows > 0)
		{

			echo '<table style="width:100%">
					<tr>
						<td style="width:68%">
						  <strong>Nombre de la Autoridad/Ente</strong>
						</td>
						<td style="width:30%">
						  <strong>Acciones</strong>
						</td>						
					</tr>	
			';
				$datos->data_seek(0);
				while ($fila = $datos->fetch_assoc()) 
				{					
							$estatus = '<img src="iconos/asemed_eliminar.png" title="Eliminar '.$fila["nombre"].'">';				


					echo '<tr class="filas">';
				    echo '<td>'. $fila['nombre'].'</td>';							    
				    echo '<td class="acciones"><p><a href="javascript:autoridades_borrar('.$fila['id_autoridad'].');">'.$estatus.'</a></p></td>';
				    echo '</tr>';
				}
			echo '</table>';
		}	
		else
			echo "No hay Autoridades Registradas";

		?>	

	</div>
</div>
