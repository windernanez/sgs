<?php
  include("includes/conexion.php"); 
  include("seguridad.php");
    
    $usuario = $_SESSION['nhsgcusuario'];
    $idusuario = $_SESSION['nhsgcidusuario'];
    
?>
<!DOCTYPE html>
<html>
<head>
<title>Marcar Salida Solicitud</title>
<style type="text/css">
body
{
	background: white;
	margin: 10px;
	font-size: 16px;
	font-family: Arial;
}
</style>
<script type="text/javascript" src="js/jquery-1.11.0.min.js"></script>
<script>
function cerrar()
{
	parent.Shadowbox.close();			
}

function listo()
{
	var control = document.elaborar.control.value;
  var tramitador = document.elaborar.tramitador.value;
	if(confirm("Marcar Salida de la solicitud N° " + control + "?"))
	{
		parent.marcar_salida(control, tramitador);
		parent.Shadowbox.close();		
	}	
}
</script>

</head>
<body>

<form name="elaborar" action="correspondencia_elaborar.php" method="post" enctype="multipart/form-data">
    <table width="100%" border="0">
      <tr>
        <td colspan="2" style="text-align:center">
          <h1 style="margin:14px">Salida de N° Control <?php echo $_GET['control']; ?></h1>
          <h2 style="margin:10px"><label for="tramitador">Tramitador:</label>
      <select id="tramitador" name="tramitador" style="font-size:.8em">
        <?php
          $em = $mysqli->query("SELECT id_usuario, nombre FROM usuarios WHERE(nivel <> 'CO' AND nivel <> 'FA') ORDER BY nombre ASC");
          while($e = $em->fetch_assoc())
          {
            if(($_SESSION['nhsgcnivel'] == "OP")&&($e['id_usuario'] == $idusuario))
              echo '<option value="'.$e['id_usuario'].'" selected>'.$e['nombre'].'</option>';
            else                
              echo '<option value="'.$e['id_usuario'].'">'.$e['nombre'].'</option>';
          }
        ?>
      </select>
</h2>
        </td>
      </tr>
      <tr>  
        <td width="50%" style="text-align:center">

          <input name="control" type="hidden" id="control" value="<?php echo $_GET['control']; ?>" />
          <input name="enviar" type="button" class="bv10" id="enviar"  value="SALIDA" style="padding:15px; font-size:1.2em"onclick="listo();" /></td>
          <td width="50%" style="text-align:center">
        	<input type="button" name="cancelar" value="CANCELAR" style="padding:15px; font-size:1.2em" onclick="cerrar();">	
          </td>
      </tr>      
    </table>
  </form>

</form>
</body>
</html>