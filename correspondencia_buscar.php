<?php
	include("includes/conexion.php");	
	session_start();
?>
<link rel="stylesheet" type="text/css" href="jsha/shadowbox.css">
	<script type="text/javascript" src="jsha/shadowbox.js"></script>
	<script type="text/javascript">
	  Shadowbox.init();
	</script>

<?php
	if(!$_SESSION['nhsgcusuario'])
	{
		$usuario = "XIODARY HERNANDEZ";
		$idusuario = "2";
	}
	else
	{
		$usuario = $_SESSION['nhsgcusuario'];
		$idusuario = $_SESSION['nhsgcidusuario'];
	}	
?>

<div class="cajas">
<div class="titulos_cajas">Buscar Solicitudes</div>
<p><label for="controlb">Nº de Control:</label><input type="text" id="controlb" placeholder="Nº de Control" autofocus><span id="editar"></span></p>
<p><label>Línea/Proyecto: </label><span class="cajitas" id="linea">-</span></p>
<p><label>Embarcación: </label><span class="cajitas" id="embarcacion">-</span></p>
<p><label>Viaje: </label><span class="cajitas" id="viaje">-</span></p>
<p><label>Asunto de Comunicación: </label><span class="cajitas" id="asunto">-</span></p>
<p><label>Autoridad: </label><span class="cajitas" id="autoridad">-</span></p>
<p><label>Fecha/Hora Salida: </label><span class="cajitas" id="hora_s">-</span></p>
<p><label>Operador: </label><span class="cajitas" id="operador_s">-</span></p>
<p><label>Fecha/Hora Retorno: </label><span class="cajitas" id="hora_r">-</span></p>
<p><label>Operador: </label><span class="cajitas" id="operador_r">-</span></p>
<p><label>Observaciones: </label><span class="cajitas" id="obs">-</span></p>
<p><label>Copia Digitalizada: </label><span class="cajitas" id="soporte">-</span></p>
<p><label>Copia Email: </label><span class="cajitas" id="soportemail">-</span></p>
<p><label>Archivo: </label><span class="cajitas" id="soportextra">-</span></p>
</div>

<div class="clr"></div>
<script type="text/javascript">
	$( "#controlb" ).keypress(function( event ) {
	  if ( event.which == 13 ) 
	  {
	     var control = $('#controlb').val();
		if(control)
			$("#procesar").load("control_buscar.php", {control: control});
	  }
	});  
</script>