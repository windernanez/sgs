
<!DOCTYPE html>
<html>
<head>
<title>Elaborando Solicitud</title>
<style type="text/css">
body
{
	background: white;
	margin: 10px;
	font-size: 16px;
	font-family: Arial;
}
</style>
<script type="text/javascript" src="js/jquery-1.11.0.min.js"></script>
<script>
function cerrar()
{
	parent.Shadowbox.close();			
}

function listo()
{
	var control = document.elaborar.control.value;
	if(confirm("Elaboró la solicitud N° " + control + "?"))
	{
		parent.marcar_elaboracion(control);
		parent.Shadowbox.close();		
	}	
}
</script>

</head>
<body>

<form name="elaborar" action="correspondencia_elaborar.php" method="post" enctype="multipart/form-data">
    <table width="100%" border="0">
      <tr>
        <td colspan="2" style="text-align:center"><h1>N° Control <?php echo $_GET['control']; ?></strong></td>
      </tr>
      <tr>  
        <td width="50%" style="text-align:center">

          <input name="control" type="hidden" id="control" value="<?php echo $_GET['control']; ?>" />
          <input name="enviar" type="button" class="bv10" id="enviar"  value="LISTO" style="padding:15px; font-size:1.2em"onclick="listo();" /></td>
          <td width="50%" style="text-align:center">
        	<input type="button" name="cancelar" value="CANCELAR" style="padding:15px; font-size:1.2em" onclick="cerrar();">	
          </td>
      </tr>      
    </table>
  </form>

</form>
</body>
</html>