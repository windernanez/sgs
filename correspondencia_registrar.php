<?php
	include("includes/conexion.php");	
	session_start();
	$error = "";
		if(($_SESSION['nhsgcnivel'] == 'AD')||($_SESSION['nhsgcnivel'] == 'OP'))
		{
			$usuario = $_SESSION['nhsgcusuario'];
			$idusuario = $_SESSION['nhsgcidusuario'];
		}	
		else
		 $error = "No tiene acceso a este módulo";	
?>

<div class="cajas">
<div class="titulos_cajas">Nueva Solicitud</div>
<?php
if($error)
{
	echo "<h2>".$error."</h2>";
	exit();
}  
?>
<p><label>Fecha de la Solicitud:</label><input type="date" id="fecha_solicitud" value="<?php echo date("Y-m-d");?>"></p>
<p><label for="linea">Línea/Proyecto:</label>
	<select id="linea">
		<option value="-1">Seleccione</option>
		<?php
			$li = $mysqli->query("SELECT * FROM lineas WHERE(activo = 1) ORDER BY nombrel ASC");
			if(!$li->num_rows)
				echo '
					<script>
						alert("Debe registrar Lineas/Proyectos para continuar")
						$("#pantallas").load("lineas_proyectos.php");
					</script>
			';


			while($l = $li->fetch_assoc())
			{
				echo '<option value="'.$l['id_linea'].'">'.$l['nombrel'].'</option>';
			}
		?>
	</select>
</p>
<p><label for="embarcacion">Embarcación:</label>
<select id="embarcacion">
	<option value="-1">Seleccione</option>
		<?php
			$em = $mysqli->query("SELECT * FROM embarcaciones WHERE(activo = 1) ORDER BY nombre ASC");
			if(!$em->num_rows)
				echo '
					<script>
						alert("Debe registrar Embarcaciones para continuar")
						$("#pantallas").load("embarcaciones.php");
					</script>
			';
			while($e = $em->fetch_assoc())
			{
				echo '<option value="'.$e['id_embarcacion'].'">'.$e['nombre'].'</option>';
			}
		?>
	</select>
</p>
<p><label for="viaje">Nº de Viaje:</label><input type="text" id="viaje" placeholder="Nº Viaje (si aplica)"></p>

<p><label for="asunto">Asunto de Comunicación:</label>
<select id="asunto">
		<option value="-1">Seleccione</option>
		<?php
			$asuntos = $mysqli->query("SELECT * FROM asuntos ORDER BY asunto ASC");
    	  	while($a = $asuntos->fetch_assoc())
			{
				echo '<option value="'.$a['asunto'].'">'.$a['asunto'].'</option>';
			}
		?>
	</select>
</p>     

<p><label for="autoridad">Autoridad:</label>
	<select id="autoridad">
		<option value="-1">Seleccione</option>
		<?php
			$em = $mysqli->query("SELECT * FROM autoridades ORDER BY nombre ASC");
			if(!$em->num_rows)
				echo '
					<script>
						alert("Debe registrar Autoridades para continuar")
						$("#pantallas").load("autoridades.php");
					</script>
			';
			while($e = $em->fetch_assoc())
			{
				echo '<option value="'.$e['nombre'].'">'.$e['nombre'].'</option>';
			}
		?>
	</select>
</p>

<p><label>Operador:</label>
		<select id="idusuario">
			<option value="-1">Seleccione</option>
			<?php
				$em = $mysqli->query("SELECT id_usuario, nombre FROM usuarios ORDER BY nombre ASC");
				if(!$em->num_rows)
					echo '
						<script>
							alert("Se deben registrar Usuarios para continuar")
							$("#pantallas").load("cartelera.php");
						</script>
				';

				while($e = $em->fetch_assoc())
				{
					echo '<option value="'.$e['id_usuario'].'">'.$e['nombre'].'</option>';
				}
			?>
		</select>
</p>
<p style="text-align:center"><input type="button" value="Registrar Solicitud" id="registrar_co"></p>
</div>
<p id="pantallas_ver"></p>
<script>
	$( "#registrar_co" ).click(function() {
	  		correspondencia_registrar();
		});	
	//$("#pantallas_ver").load("correspondencia_ver.php");
</script>
<div class="clr"></div>

