<?php
include("includes/conexion.php");
include("seguridad.php")
?>
<div style="width:960px; margin:0 auto">
   <p style="border: 1px solid grey; padding: 5px">
      <label for="desde">Código Profit:</label>
      <input type="text" id="codcli" autofocus  placeholder="Código Profit del Cliente" autofocus onKeyup="javascript:return pasarMayusculas(this.value, this.id)">
   </p>
</div>

<div id="reporte_ver" style="text-align:center"></div>

<script type="text/javascript">
   $( "#codcli" ).blur(function() {
      var codcli = $("#codcli").val();
      if(codcli !== '')
      {
         $("#reporte_ver").html('<img src="loading.gif" height="40"><br>Buscando...');
         $("#reporte_ver").load("ver_expediente.php", {codcli: codcli});
      }
   });
</script>