<!DOCTYPE html>
<html>
<head>
<title>Quitar Adjunto</title>
<style type="text/css">
body
{
	background: white;
	margin: 10px;
	font-size: 12px;
	font-family: Arial;
}
</style>
<script type="text/javascript" src="js/jquery-1.11.0.min.js"></script>
<script>
function cerrar()
{
	parent.Shadowbox.close();
}

function listo()
{
	var control = document.proceder.control.value;
    var archivo = document.proceder.archivo.value;
	if(confirm("Quitar adjunto " + archivo + "?"))
	{
		parent.quitar_adjunto(control);
		parent.Shadowbox.close();
	}
}
</script>

</head>
<body>

   <form name="proceder" action="quitar_doc.php" method="post" enctype="multipart/form-data">
    <table width="100%" border="0">
      <tr>
         <td colspan="2" style="text-align:center"><h1>Quitar adjunto <b><?php echo $_GET['archivo']; ?></b></h1></td>
      </tr>
      <tr>
        <td width="50%" style="text-align:center">

          <input name="control" type="hidden" id="control" value="<?php echo $_GET['nro']; ?>" />
          <input name="archivo" type="hidden" id="archivo" value="<?php echo $_GET['archivo']; ?>" />
          <input name="enviar" type="button" class="bv10" id="enviar"  value="QUITAR" style="padding:15px; font-size:1.2em"onclick="listo();" /></td>
          <td width="50%" style="text-align:center">
        	<input type="button" name="cancelar" value="CANCELAR" style="padding:15px; font-size:1.2em" onclick="cerrar();">
          </td>
      </tr>
    </table>
  </form>

</form>
</body>
</html>