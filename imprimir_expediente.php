<?php
error_reporting(~0);
ini_set('display_errors',1);

require_once('fpdf/fpdf.php');
require_once('fpdi/fpdi.php');

class ConcatPdf extends FPDI
{
    public $files = array();

    public function setFiles($files)
    {
        $this->files = $files;
    }

    public function concat()
    {
        foreach($this->files AS $file) {
            $pageCount = $this->setSourceFile($file);
            for ($pageNo = 1; $pageNo <= $pageCount; $pageNo++) {
                $tplIdx = $this->ImportPage($pageNo);
                $s = $this->getTemplatesize($tplIdx);
                $this->AddPage($s['w'] > $s['h'] ? 'L' : 'P', array($s['w'], $s['h']));
                $this->useTemplate($tplIdx);
            }
        }
    }
}

$pdf = new ConcatPdf();
$pdf->addPage();
$pdf->Image("imagenes/logo.jpg", 8, 4,25);
$pdf->SetFont('Arial','B',16);
$pdf->setX(35);
$pdf->Cell(40,10,utf8_decode('NH Agentes Aduanales y Navieros'));
$pdf->ln();
$pdf->setY($pdf->getY()+10);
$pdf->Cell(40,10,utf8_decode('Información del Cliente: '.$_GET["cod"]));
$pdf->ln();
$pdf->SetFont('Arial','',12);
$pdf->Cell(40,10,utf8_decode('Si se quiere habilitar esta opción entonces los documentos de los clientes se deben cargar'));
$pdf->ln();
$pdf->cell(40,10,'como imagen y no como PDF!!!');
$pdf->SetFont('Arial','B',16);
$pdf->ln(10);
$pdf->cell(40,10,"Solicitar formato de expediente");
$pdf->setFiles(array('adjuntos/one.pdf', 'adjuntos/two.pdf', 'adjuntos/three.pdf'));
$pdf->concat();

$pdf->Output('concat.pdf', 'I');
?>