<div class="cajas">
   <div class="titulos_cajas">Registrar Documentos a Consignar por el Cliente</div>
   <p><label for="nomdoc">Nombre del Documento:</label><input type="text" id="documento" placeholder="Tipo de Documento" autofocus onKeyup="javascript:return pasarMayusculas(this.value, this.id)"></p>
   <p><label for="abrdoc">Abreviatura del Doc:</label><input type="text" id="abrdoc" placeholder="Nombre corto para el documento" maxlength="10" onKeyup="javascript:return pasarMayusculas(this.value, this.id)"></p>
   <p><label for="vence">¿Doc. Vence?</label>
      <select id="fvenc">
         <option value="1">SI</option>
         <option value="0">NO</option>
      </select>
   </p>
   <p><label for="docreq">¿Doc. Requerido?</label>
      <select id="docreq">
         <option value="1">SI</option>
         <option value="0">NO</option>
      </select>
   </p>
   <p style="text-align:center"><input type="button" value="Registrar" id="registrar_docs"></p>
</div>
<p id="pantallas_ver"></p>
<script>
   $( "#registrar_docs" ).click(function() {
      documentos_registrar();
   });

   $("#pantallas_ver").load("docs_ver.php");
</script>