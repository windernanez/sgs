<div class="cajas">
<div class="titulos_cajas">Registrar Embarcación</div>
<p><label for="nombre">Embarcación:</label><input type="text" id="nombre" placeholder="Nombre/Identificación" autofocus></p>
<p><label for="imo">IMO:</label><input type="text" id="imo" placeholder="IMO"></p>
<p><label for="matricula">Matricula:</label><input type="text" id="matricula" placeholder="Matricula"></p>
<p><label for="bandera">Bandera:</label><input type="text" id="bandera" placeholder="Bandera"></p>
<p><label for="tonelaje">Tonelaje Bruto:</label><input type="text" id="tonelaje" placeholder="Tonelaje Bruto"></p>
<p><label for="neto">Tonelaje Neto:</label><input type="text" id="neto" placeholder="Tonelaje Neto"></p>
<p><label for="eslora">Eslora:</label><input type="text" id="eslora" placeholder="Eslora"></p>
<p style="text-align:center"><input type="button" value="Registrar" id="registrar_em"></p>
</div>
<p id="pantallas_ver"></p>
<script>
	$( "#registrar_em" ).click(function() {
	  		embarcacion_registrar();
		});	
	$("#pantallas_ver").load("embarcaciones_ver.php");
</script>
<div class="clr"></div>


