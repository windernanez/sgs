<?php
	include("includes/conexion.php");
	session_start();
	$error = "";
		if(($_SESSION['nhsgcnivel'] == 'AD')||($_SESSION['nhsgcnivel'] == 'OP'))
		{
			$usuario = $_SESSION['nhsgcusuario'];
			$idusuario = $_SESSION['nhsgcidusuario'];
		}
		else
		 $error = "No tiene acceso a este módulo";
?>
<link rel="stylesheet" type="text/css" href="jsha/shadowbox.css">
<script type="text/javascript" src="jsha/shadowbox.js"></script>
<script type="text/javascript">
	  Shadowbox.init();
</script>

<div class="cajas">
    <div class="titulos_cajas">Registrar Cliente</div><?php
    if($error)
    {
        echo "<h2>".$error."</h2>";
        exit();
    } ?>

    <p><label>Código Profit:</label><input type="text" id="codigocli" onKeyup="javascript:return pasarMayusculas(this.value, this.id)" placeholder="Código Profit del Cliente"></p>
    <p><label for="rifcli">RIF Cliente:</label><input type="text" id="rifcli" onKeyup="javascript:return FormatoRIF(event, this.id, this.value)" maxlength="12" placeholder="J-00000000-0"></p>
    <p><label for="nomcli">Nombre:</label><input type="text" id="nomcli" onKeyup="javascript:return pasarMayusculas(this.value, this.id)" placeholder="Nombre del Cliente"></p>
    <!--<p><label>Fecha Venc. RIF:</label><input type="date" id="fecha_vencrifcli" value="<?php echo date("Y-m-d");?>"></p>-->
    <p><label for="cedoto">Cédula Otorgante:</label>
       <select id="nac" style="width: 50px;">
         <option value="V">V</option>
         <option value="E">E</option>
       </select>
       <input style="width: 180px;" type="text" id="cedoto" onKeypress="javascript:return SoloNumeros(event)" maxlength="8" placeholder="Solo números"></p>
    <p><label for="rifoto">RIF Otorgante:</label><input type="text" id="rifoto" onKeyup="javascript:return FormatoRIF(event, this.id, this.value)" maxlength="12" placeholder="J-00000000-0"></p>
    <p><label for="nomoto">Nombre Otorg:</label><input type="text" id="nomoto" onKeyup="javascript:return pasarMayusculas(this.value, this.id)" placeholder="Nombre del Otorgante del Poder"></p>

    <div class="titulos_cajas">Datos Persona de Contacto</div>
      <p><label for="lcontacto">Nombre:</label><input type="text" id="contacto" onKeyup="javascript:return pasarMayusculas(this.value, this.id)" placeholder="Nombre de la persona de contacto"></p>
      <p><label for="lcorreo">Correo:</label><input type="email" id="correo" placeholder="alguien@ejemplo.com"></p><br>
    <br><p style="text-align:center"><input type="button" value="Registrar Cliente" id="registrar_cli"></p>
</div>
<p id="pantallas_ver"></p>
<script>
	$( "#registrar_cli" ).click(function() {
	  		cliente_registrar();
		});
	//$("#pantallas_ver").load("correspondencia_ver.php");
</script>
<div class="clr"></div>

