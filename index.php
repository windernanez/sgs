<?php
	include("seguridad.php");
?>
<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<title>NH Sistema de Gestión de Solicitudes</title>
	<link rel="stylesheet" type="text/css" href="normalize.css">
	<link rel="stylesheet" type="text/css" href="estilos.css">
        <script type="text/javascript" src="js/jquery-1.11.0.min.js"></script>
        <link href="datepicker/css/jquery.datepick.css" rel="stylesheet">
         <script src="datepicker/js/jquery.plugin.min.js"></script>
         <script src="datepicker/js/jquery.datepick.js"></script>
	<script type="text/javascript" src="js/funciones.js"></script>
	<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
	<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
	<link rel="stylesheet" type="text/css" href="jsha/shadowbox.css">
	<script type="text/javascript" src="jsha/shadowbox.js"></script>
	<script type="text/javascript">
	  Shadowbox.init();
	</script>

	<script>
	function correspondencia_registrar()
	{
		var asunto = $("#asunto").val();

		if(asunto != "-1")
		{

			var linea = $("#linea").val();
			var embarcacion = $("#embarcacion").val();
			var viaje = $("#viaje").val();
			var idusuario = $("#idusuario").val();
			var autoridad = $("#autoridad").val();
			var fecha_s = $("#fecha_solicitud").val();
			//var tcontrol = $("#tcontrol").val();

			if((linea == -1)||(embarcacion == -1)||(autoridad == -1)||(idusuario == -1))
			{
				alert("Línea, Embarcación, Autoridad y Usuario Asignado son Obligatorios");
			}
			else
			{

			//if(tcontrol == "m")
			//	var control = $("#control").val();
			//else
				var control = 0;

			$("#procesar").load("correspondencia_registrar_bd.php", {fecha_s: fecha_s, linea: linea, embarcacion: embarcacion, viaje: viaje, idusuario: idusuario, asunto: asunto, autoridad: autoridad, control: control});

			}

		}
		else
		{
			alert("Debe indicar un Asunto de Comunicación");
			$("#asunto").focus();
		}

	}

	function correspondencia_retornar()
	{
		var adjunto = $("#adjunto").val();
		var adjuntomail = $("#adjuntomail").val();
		var adjuntoextra = $("#adjuntoextra").val();

		if(adjunto)
		{
			var control = $("#controlr").val();
			var obs = $("#obs").val();
			var idusuario = $("#idusuario").val();
			var naduana = $("#naduana").val();


			if(control)
				$("#procesar").load("correspondencia_retornar_bd.php", {control: control, obs: obs, idusuario: idusuario, adjunto: adjunto, adjuntomail: adjuntomail, adjuntoextra: adjuntoextra, naduana: naduana});
			else
			{
				alert("Recuerde indicar el Número de Control");
				$("#control").focus();
			}
		}
		else
		{
			alert("Es obligatorio adjuntar copia digitalizada del documento");
			$("#control").focus();
		}

	}

        function procesar_docs_cli(edita)
	{
           var codcli = $("#codcli").val();
           var params = {codcli: codcli, edita: edita};

            $("[id^=valoradj_]").each(function(){
              params[this.id] = this.value;
            });

            $("[id^=fvenc_]").each(function(){
              params[this.id] = this.value;
            });

            $("#procesar").load("adjuntar_docs_bd.php", params);
	}

        function ver_doc_adjunto_cliente(archivo, abr)
	{
           var enlace = encodeURI(archivo);
		$("#valoradj_"+abr).val(archivo);
                //$("#ver_"+abr).html(archivo);
		$("#ver_"+abr).html('<a href="' + enlace + '" target="_blank">Ver Adjunto</a>');
	}

   function correspondencia_adjunto(archivo)
	{
		$("#adjunto").val(archivo);
		$("#veradjunto").html('<a href="adjuntos/' + archivo + '" target="_blank">Ver Adjunto</a>');
	}

	function correspondencia_adjuntob(archivo)
	{
		$("#adjunto").val(archivo);
		$("#soporte").html('<a href="adjuntos/' + archivo + '" target="_blank">Ver Adjunto</a>');
	}

	function correspondencia_adjuntomail(archivo)
	{
		$("#adjuntomail").val(archivo);
		$("#veradjuntomail").html('<a href="adjuntosmail/' + archivo + '" target="_blank">Ver Email</a>');
	}

	function correspondencia_adjuntomailb(archivo)
	{
		$("#adjuntomail").val(archivo);
		$("#soportemail").html('<a href="adjuntosmail/' + archivo + '" target="_blank">Ver Email</a>');
	}

	function correspondencia_adjuntoextra(archivo)
	{
		$("#adjuntoextra").val(archivo);
		$("#veradjuntoextra").html('<a href="adjuntosextra/' + archivo + '" target="_blank">Ver Adjunto</a>');
	}

	function correspondencia_adjuntoextrab(archivo)
	{
		$("#adjuntoextra").val(archivo);
		$("#soportextra").html('<a href="adjuntosextra/' + archivo + '" target="_blank">Ver Adjunto</a>');
	}

    function cliente_adjuntor(archivo)
	{
		$("#rifadjunto").val(archivo);
		$("#verrcadjunto").html('<a href="adjuntos/Expediente del Cliente/' + archivo + '" target="_blank">Ver Adjunto</a>');
	}


	function usuarios_registrar()
	{
		var nombre = $("#nombre").val();
		var email = $("#email").val();
		var nivel = $("#nivel").val();
		var codigo = $("#codigo").val();
		var clave = $("#clave").val();

		$("#procesar").load("usuarios_bd.php", {nombre: nombre, email: email, nivel: nivel, codigo: codigo, clave: clave});
	}

	function lp_registrar()
	{
		var nombre = $("#nombre").val();
		var tipo = $("#tipo").val();

		$("#procesar").load("lineas_proyectos_bd.php", {nombre: nombre, tipo: tipo});
	}

	function lp_borrar(lp)
	{
		if(confirm("Desea borrar esta Línea/Proyecto?"))
		{
			$("#procesar").load("lineas_proyectos_bd_borrar.php", {lp: lp});
		}
	}

	function embarcacion_registrar()
	{
		var nombre = $("#nombre").val();
		var imo = $("#imo").val();
		var matricula = $("#matricula").val();
		var bandera = $("#bandera").val();
		var tonelaje = $("#tonelaje").val();
		var neto = $("#neto").val();
		var eslora = $("#eslora").val();

		$("#procesar").load("embarcaciones_bd.php", {nombre: nombre, imo: imo, matricula: matricula, bandera: bandera, tonelaje: tonelaje, neto: neto, eslora: eslora});
	}

	function embarcacion_borrar(embarcacion)
	{
		if(confirm("Desea borrar esta Embarcación?"))
		{
			$("#procesar").load("embarcaciones_bd_borrar.php", {embarcacion: embarcacion});
		}
	}

	function autoridad_registrar()
	{
		var nombre = $("#nombre").val();

		$("#procesar").load("autoridades_bd.php", {nombre: nombre});
	}

	function autoridades_borrar(autoridad)
	{
		if(confirm("Desea borrar esta Autoridad?"))
		{
			$("#procesar").load("autoridades_bd_borrar.php", {autoridad: autoridad});
		}
	}

	function asunto_registrar()
	{
		var asunto = $("#asunto").val();

		$("#procesar").load("asuntos_bd.php", {asunto: asunto});
	}

	function asuntos_borrar(asunto)
	{
		if(confirm("Desea borrar este Asunto de Comunicación?"))
		{
			$("#procesar").load("asuntos_bd_borrar.php", {asunto: asunto});
		}
	}

        function documentos_registrar()
	{
		var doc = $("#documento").val();
                var abrdoc = $("#abrdoc").val();
                var fvenc = $("#fvenc").val();
                var docreq = $("#docreq").val();

                if (doc === '' || abrdoc === '')
                {
                   alert('Nombre del documento y Abreviatura del documento son obligatorios');
                   return false;
                }
		$("#procesar").load("documentos_bd.php", {doc: doc, abrdoc: abrdoc, fvenc: fvenc, docreq: docreq});
	}

        function documentos_borrar(doc)
	{
		if(confirm("Desea borrar este Documento?"))
		{
			$("#procesar").load("documentos_bd_borrar.php", {doc: doc});
		}
	}


	function comision_registrar()
	{
		var asunto = $("#asunto").val();
		var autoridad = $("#autoridad").val();

		$("#procesar").load("comisiones_bd.php", {asunto: asunto, autoridad: autoridad});
	}

   function cliente_registrar()
   {
      var codigo   = $("#codigocli").val();
      var rifcli   = $("#rifcli").val();
      var nomcli   = $("#nomcli").val();
      var nac      = $("#nac").val();
      var cedoto   = $("#cedoto").val();
      var rifoto   = $("#rifoto").val();
      var nomoto   = $("#nomoto").val();
      var contacto = $("#contacto").val();
      var correo   = $("#correo").val();

      if(codigo === '')
      {
         alert('Código Profit es requerido');
         $("#codigocli").focus();
         return false;
      }

      if(rifcli === '')
      {
         alert('Debe indicar el RIF');
         $("#rifcli").focus();
         return false;
      }

      if(nomcli === '')
      {
         alert('Debe indicar el nombre del Cliente');
         $("#nomcli").focus();
         return false;
      }

      if(cedoto === '')
      {
         alert('Indique la cédula del otorgante del poder');
         $("#cedoto").focus();
         return false;
      }

      if(rifoto === '')
      {
         alert('Indique el RIF del otorgante del poder');
         $("#rifoto").focus();
         return false;
      }

      if(nomoto === '')
      {
         alert('Indique el nombre del otorgante del poder');
         $("#nomoto").focus();
         return false;
      }

      if(contacto === '' || correo === '')
      {
         alert('Debe indicar la información de la persona de contacto');
         return false;
      }

      $("#pantallas_ver").load("clientes_bd.php", {codigo: codigo, rifcli: rifcli, nomcli: nomcli, nac: nac, cedoto: cedoto, rifoto: rifoto, nomoto: nomoto, contacto: contacto, correo: correo});
   }

	function comisiones_borrar(comision)
	{
		if(confirm("Desea borrar este Registro?"))
		{
			$("#procesar").load("comisiones_bd_borrar.php", {comision: comision});
		}
	}


	function asuntos_comisionable(asunto, actual)
	{
		if(actual == 0)
		{
			if(confirm("Desea activar este Asunto de Comunicación para pagos de comisión?"))
			{
				$("#procesar").load("asuntos_bd_comisionable.php", {asunto: asunto, actual: actual});
			}
		}
		else
		{
			if(confirm("Desea desactivar este Asunto de Comunicación para pagos de comisión?"))
			{
				$("#procesar").load("asuntos_bd_comisionable.php", {asunto: asunto, actual: actual});
			}
		}

	}
	function activar_desactivar(usuario, estatus)
	{
		var txt = "";
		if(estatus == 1)
			txt = "desactivar";
		else
			txt = "activar";

		if (confirm('Desea ' + txt + ' el Usuario?'))
		$("#procesar").load("usuarios_desactivar_activar.php", {usuario: usuario, estatus: estatus}, function(){ $("#pantallas_ver").load("usuarios_ver.php");});
	}

	function reportes_ver()
	{
		$("#reporte_ver").html('<img src="loading.gif" height="40"><br>Buscando...');

		var embarcacion = $("#embarcacion_re").val();
		var linea = $("#linea_re").val();
		var desde = $("#desde").val();
		var hasta = $("#hasta").val();
		var asunto = $("#asunto").val();
		var autoridad = $("#autoridad").val();
		var xcontrol = $("#xcontrol").val();

		$("#reporte_ver").load("correspondencia_reporte_ver.php", {desde: desde, hasta: hasta, linea: linea, embarcacion: embarcacion, asunto: asunto, autoridad: autoridad, xcontrol: xcontrol})
	}


	function perfil_ver()
	{
		$("#perfil_ver").html('<img src="loading.gif" height="40"><br>Buscando...');

		var usuario = $("#usuario").val();
		var tipou = $("#tipou").val();
		var desde = $("#desdep").val();
		var hasta = $("#hastap").val();

		$("#perfil_ver").load("correspondencia_perfil_ver.php", {desde: desde, hasta: hasta, usuario: usuario, tipou: tipou});
	}

	function buscar_control()
	{
		var control = $('#controlb').val();
		if(control)
			$("#procesar").load("control_buscar.php", {control: control})
	}

	$(document).on('blur', "#control", function() {
		var control = $('#control').val();
		if(control)
			$("#procesar").load("control_validar.php", {control: control})
	});
	$(document).on('blur', "#controlr", function() {
		var control = $('#controlr').val();
		if(control)
			$("#procesar").load("control_validar_retorno.php", {control: control})
	});

	$(document).on('blur', "#controlb", function() {
		var control = $('#controlb').val();
		if(control)
			$("#procesar").load("control_buscar.php", {control: control})
	});

	$(document).on('change', "#embarcacion_re",function() {
      reportes_ver()
	});


	$(document).on('change', "#linea_re",function() {
      reportes_ver()
	});

	$(document).on('change', "#desde",function() {
      reportes_ver()
	});

	$(document).on('change', "#hasta",function() {
      reportes_ver()
	});

	$(document).on('change', "#autoridad",function() {
      reportes_ver()
	});

	$(document).on('change', "#asunto",function() {
      reportes_ver()
	});

	$(document).on('change', "#filtro",function() {
      cartelera_ver()
	});


	$(document).on('change', "#tipou",function() {
      perfil_ver()
	});

	$(document).on('change', "#usuario",function() {
      perfil_ver()
	});

	$(document).on('change', "#desdep",function() {
      perfil_ver()
	});

	$(document).on('change', "#hastap",function() {
      perfil_ver()
	});


	function adjuntar_copia(control)
	{
		Shadowbox.open( { content:   "correspondencia_retornar_adjunto.php?control=" + control,
	                    type:        "iframe",
	                    player:      "iframe",
	                    title:         "Adjuntar Copia",
	                    options:   {   initialHeight:100,
	                                    initialWidth:450,
	                                    loadingImage:"loading.gif",
	                                    handleUnsupported:  'link'
	                                }
	                 });
	}

	function adjuntar_copiamail(control)
	{
		Shadowbox.open( { content:   "correspondencia_retornar_adjuntomail.php?control=" + control,
	                    type:        "iframe",
	                    player:      "iframe",
	                    title:         "Adjuntar Email",
	                    options:   {   initialHeight:100,
	                                    initialWidth:450,
	                                    loadingImage:"loading.gif",
	                                    handleUnsupported:  'link'
	                                }
	                 });
	}

	function editar_solicitud(control)
	{
		Shadowbox.open( { content:   "editar_solicitud.php?control=" + control,
	                    type:        "iframe",
	                    player:      "iframe",
	                    title:         "Modificar Solicitud",
	                    options:   {   initialHeight:600,
	                                    initialWidth:450,
	                                    loadingImage:"loading.gif",
	                                    handleUnsupported:  'link'
	                                }
	                 });
	}

	function cartelera()
	{
		$("#pantallas").load("cartelera.php");
	}

	function cartelera_ver()
	{
		$("#cartelera_ver").html('<img src="loading.gif" height="40"><br>Buscando...');

		var filtro = $("#filtro").val();

		$("#cartelera_ver").load("cartelera_ver.php", {filtro: filtro});
	}

	function facturar(control)
	{
		$("#procesar").load("facturar.php", {control: control});
	}

	function ver_usuarios()
	{
		$("#pantallas").load("usuarios.php");
	}
	</script>

</head>

<body>
	<div id="pagina">

		<header>
			<div id="logo">
				<a href="javascript:cartelera();"><img src="imagenes/logo.jpg" height="100" alt="Logo NH"></a>
			</div>
			<div id="titulo">
				<h1>Sistema de Gestión de Solicitudes</h1>
			</div>
			<div id="login" style="float:right; font-size:12px;">Ha iniciado Sesión como <strong><?php echo $_SESSION['nhsgcusuario'];?></strong> | <a href="cerrar_sesion.php">Cerrar Sesión</a></div>
			<nav>
				<ul>
					<?php
						if(($_SESSION['nhsgcnivel'] != "CO")&&($_SESSION['nhsgcnivel'] != "SO"))
						{
					?>
					<li id="btncartelera">Cartelera</li>
					<?php
						}
					?>
					<li>Solicitudes
						<ul>
							<?php
							if(($_SESSION['nhsgcnivel'] != "CO")&&($_SESSION['nhsgcnivel'] != "SO"))
							{
							?>
							<li id="btnnueva">Nueva</li>
							<li id="btnretornar">Retornar</li>
							<?php
							}
							?>
							<li id="btnbuscar">Buscar</li>
							<li id="btnreportes">Reporte</li>
							<?php
							if(($_SESSION['nhsgcnivel'] == "AD")||($_SESSION['nhsgcnivel'] == "CO"))
							{
							?>
							<li id="btnperfil">Perfil</li>
							<?php
							}
							?>
						</ul>
					</li>
							<li>Clientes
						<ul>
							<?php
							if(($_SESSION['nhsgcnivel'] != "CO")&&($_SESSION['nhsgcnivel'] != "SO"))
							{
							?>
							<li id="btnregcli">Registro</li>
                                                        <li id="btndoc">Documentación</li>
							<?php
							}
							?>
							<li id="btnbuscarcli">Expediente</li>
						</ul>
					</li>

					<?php
					if(($_SESSION['nhsgcnivel'] == "SO")||($_SESSION['nhsgcnivel'] == "AD"))
					{

					?>
					<li>Configuración
						<ul>
							<?php
							if($_SESSION['nhsgcnivel'] == "SO")
							{
							?>
							<li id="btnusuarios">Usuarios</li>
							<?php
							}
							?>
							<li id="btnlineas">Líneas/Proyectos</li>
							<li id="btnembarcaciones">Embarcaciones</li>
							<li id="btnautoridades">Autoridades</li>
							<li id="btnasuntos">Asuntos</li>
							<li id="btndocs">Documentos</li>
							<li id="btncomisiones">Comisiones</li>
						</ul>
					</li>
					<?php
					}
					?>


				</ul>
			</nav>
		</header>
<div style="clear:both;height:5px"></div>
		<section id="procesar">


		</section>

		<section id="pantallas">


		</section>

	<div class="clr"></div>

	</div>

<script>

	$( "#btnnueva" ).click(function() {
  		$("#pantallas").load("correspondencia_registrar.php");
	});

	$( "#btnretornar" ).click(function() {
  		$("#pantallas").load("correspondencia_retornar.php");
	});
	$( "#btnbuscar" ).click(function() {
  		$("#pantallas").load("correspondencia_buscar.php");
	});
	$( "#btnreportes" ).click(function() {
  		$("#pantallas").load("correspondencia_reporte.php");
	});
	$( "#btnusuarios" ).click(function() {
  		$("#pantallas").load("usuarios.php");
	});
	$( "#btnlineas" ).click(function() {
  		$("#pantallas").load("lineas_proyectos.php");
	});
	$( "#btnembarcaciones" ).click(function() {
  		$("#pantallas").load("embarcaciones.php");
	});
	$( "#btnautoridades" ).click(function() {
  		$("#pantallas").load("autoridades.php");
	});
	$( "#btnasuntos" ).click(function() {
  		$("#pantallas").load("asuntos.php");
	});
        $( "#btndocs" ).click(function() {
  		$("#pantallas").load("documentos.php");
	});

        $( "#btnbuscarcli" ).click(function() {
  		$("#pantallas").load("expediente_cliente.php");
	});
	$( "#btnperfil" ).click(function() {
  		$("#pantallas").load("correspondencia_perfil.php");
	});
	$( "#btncomisiones" ).click(function() {
  		$("#pantallas").load("comisiones.php");
	});
<?php
	if(($_SESSION['nhsgcnivel'] != "CO")&&($_SESSION['nhsgcnivel'] != "SO"))
	{
?>
	$("#pantallas").load("cartelera.php");
<?php
	}
?>
	$( "#btncartelera" ).click(function() {
  		cartelera();
	});

	$( "#btnregcli" ).click(function() {
  		$("#pantallas").load("nuevocliente.php");
	});

        $( "#btndoc" ).click(function() {
  		$("#pantallas").load("documentos_cliente.php");
	});
	</script>

</body>
</html>