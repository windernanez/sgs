<?php
	include("includes/conexion.php");	
?>
<div class="cajas">
	<div class="titulos_cajas">Líneas/Proyectos Registrados</div>

					
		<?php
		$datos = $mysqli->query("SELECT * FROM lineas WHERE(activo = 1) ORDER BY nombrel ASC");	

		if($datos->num_rows > 0)
		{

			echo '<table>
					<tr>
						<td style="width:62%">
						  <strong>Nombre de la Línea/Proyecto</strong>
						</td>
						<td style="width:25%; text-align:center;">
						  <strong>Tipo (PRoyecto/LInea)</strong>
						</td>						
						<td style="width:25%">
						  <strong>Acciones</strong>
						</td>						
					</tr>	
			';
				$datos->data_seek(0);
				while ($fila = $datos->fetch_assoc()) 
				{					
							$estatus = '<img src="iconos/asemed_eliminar.png" title="Click para Eliminar">';				


					echo '<tr class="filas">';
				    echo '<td>'. $fila['nombrel'].'</td>';			
				    echo '<td style="text-align:center">'. $fila['tipo'].'</td>';
				    echo '<td class="acciones"><p><a href="javascript:lp_borrar('.$fila['id_linea'].');">'.$estatus.'</a></p></td>';
				    echo '</tr>';
				}
			echo '</table>';
		}	
		else
			echo "No hay Líneas o Proyectos Registrados";

		?>	

	</div>
</div>
