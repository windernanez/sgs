<script type="text/javascript" src="jsha/shadowbox.js"></script>
<script>
//    Shadowbox.init();
    function quitar_adjunto(control)
	{
		$("#procesar").load("eliminar_adjunto.php",{control: control});
	}
</script>
<?php
include("includes/conexion.php");
require_once ("funciones.class.php");
$ver = $mysqli->query("SELECT * FROM clientes WHERE ('$_POST[codcli]' = codigo) LIMIT 1");

if(!$ver->num_rows)
{ ?>
   <script>
      $("#codcli").val("");
      $("#codcli").focus();
      alert('Cliente NO Encontrado');
   </script><?php
}//if
else
{
    $c = $ver->fetch_assoc();
    echo '<div class="titulos_cajas" id="control_docs">Control de Vigencia y Adjuntos</div>';
    $li = $mysqli->query("SELECT * FROM documentos WHERE(activo = 1) ORDER BY requerido DESC, tipo_doc ASC");
   if(!$li->num_rows)
      echo '
         <script>
            alert("Debe registrar Documentos para continuar")
            $("#pantallas").load("documentos.php");
         </script>';

   while($l = $li->fetch_assoc())
   { //echo $l['id_doc'].' > '.$l['tipo_doc'].'<br>';
      Funciones::BuscarDatos("SELECT * FROM docs_cliente WHERE clientes_id=".$c["id_cliente"]." and documentos_id=".$l["id_doc"]." LIMIT 1", $dcli);
      if(count($dcli) >0)
       {
         $dcli = $dcli->fetch_assoc();
           $separa = explode("/",$dcli["ruta"]);
            echo '<p><label><b>'.($l['fvenc'] ? 'F. Venc. ' : '').$l['tipo_doc'].':</b></label><br>';
            if($l['fvenc']):
               echo '<br><input type="text" maxlength="0" id="fvenc_'.strtolower($l['abr_doc']).'" style="width: 150px;" value="'.$dcli["fecha_venc"].'">    ';
            endif;
            echo '<a href="'.$dcli["ruta"].'" id="'. strtolower($l["abr_doc"]).'" target="_blank">Ver Adjunto</a>  |  ';
            echo '<a href="quitar_doc.php?nro='.$dcli["id"].'&archivo='.$separa[3].'" id="del_'. strtolower($l["abr_doc"]).'" rel="shadowbox;width=400;height=250">Quitar Adjunto</a><br>';
            echo '</p><hr class="cajas">';
       }
       else
       { ?>
            <p>
               <label><b><?php echo ($l['fvenc'] ? 'F. Venc. ' : '').$l['tipo_doc'] ?>:</b></label><br><?php
               if($l['fvenc']): ?>
               <input type="text" maxlength="0" id="fvenc_<?php echo strtolower($l['abr_doc']) ?>" style="width: 150px;"><?php
               endif; ?>
               <a style="display: none;" href="adjuntar_doc_cliente.php?control=x" rel="shadowbox;width=450;height=100" id="<?php echo 'link_'.strtolower($l['abr_doc']) ?>">Adjuntar Archivo</a> |
               <span id="ver_<?php echo strtolower($l['abr_doc']) ?>">Sin Archivo</span>
               <input type="hidden" id="valoradj_<?php echo strtolower($l['abr_doc']) ?>" value="">
               <?php
               if($l['requerido'] == '1')
               { ?>
               <span><font color="red">(*) Requerido</font></span><?php
               } ?>
            </p><hr class="cajas"><?php
       }
   }
?>
<p style="text-align:center"><input type="button" value="Guardar" id="guardar_docs_cli"></p>
</div>
<p id="pantallas_ver"></p>
<div class="clr"></div>
   <script>
      $("#rif").html("<?php echo $c['rif'];?>");
      $("#nombre").html("<?php echo $c['nombre'];?>");
      $("#rifoto").html("<?php echo $c['rif_otorgante'];?>");
      $("#cedoto").html("<?php echo $c['cedula_otorgante'];?>");
      $("#nomoto").html("<?php echo $c['nombre_otorgante'];?>");

      $("[id^=link]").each(function(){
            var codigo = $("#codcli").val();
            var abrdoc = (this.id).substr(5);
//            var valor  = $("#"+abrdoc).val();

            $(this).attr("href", "adjuntar_doc_cliente.php?codigo=" + codigo + "&abrdoc=" + abrdoc);
            $(this).show();
         });
         Shadowbox.init();

         $("[id^=fvenc_]").each(function(){
            $("#"+this.id).datepick({dateFormat: 'yyyy-mm-dd'});
         });

         $( "#guardar_docs_cli" ).click(function() {
      if($("#codcli").val() === '')
      {
         alert('Debe indicar un cliente');
         $("#codcli").focus();
         return false;
      }
      procesar_docs_cli(0);
   });
   </script>
<?php
}//else
?>