<?php
//include("includes/conexion.php");
class Funciones
{
   public static function BuscarDatos($sql,&$output)
   {
      $mysqli = new mysqli("localhost", "root", "", "sgs");
      $rs = $mysqli->query($sql);

      if($rs->num_rows)
      {
         $output = $rs;//$rs->fetch_assoc();
         return true;
      }
      else
         $output = array();

//      if (count($output)>0) return true; else return false;
      return false;
  }
  /*
   * @name: Buscar Cliente
   * @params: campo = Nombre del campo en la base de datos por el cual se buscará, valor=valor a buscar en la bd
   * @return: Array con los datos encontrados, en caso de no encontrar nada devuelve un array vacio
   */
   public static function buscarCliente($campo="", $valor="")
   {
        self::BuscarDatos("select * from clientes where ".$campo."='".$valor."' LIMIT 1", $output);

        return $output;
   }

   public static function buscarDocsPorCliente($id_cliente=NULL)
   {
      self::BuscarDatos("SELECT * FROM docs_cliente WHERE clientes_id=".$id_cliente, $output);

      return $output;
   }

   public static function DocsPorConsignar($id_cliente=NULL)
   {
      self::BuscarDatos("select * from documentos where id_doc not in (select documentos_id from docs_cliente where clientes_id=".$id_cliente.") and requerido='1'", $output);

      return $output;
   }

   public static function DocsVencidos($id_cliente=NULL)
   {
      self::BuscarDatos("select datediff(fecha_venc, curdate()) as diferencia, a.id, a.clientes_id as id_cli, b.tipo_doc, a.fecha_venc from docs_cliente a, documentos b where a.documentos_id=b.id_doc and clientes_id=".$id_cliente." and b.fvenc='1' and datediff(fecha_venc, curdate()) <= 100", $output);

      return $output;
   }
}
?>
