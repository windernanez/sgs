<?php
	include("includes/conexion.php");	

	if($_POST['modificar_usuario'])
	{
		
		if($_POST['clave'])
		{
			$clave = md5($_POST['clave']);
			$mysqli->query("UPDATE usuarios SET email = '$_POST[email]', nombre = '$_POST[nombre]', nivel = '$_POST[nivel]', codigo = '$_POST[codigo]', clave = '$clave' WHERE(id_usuario = '$_POST[usuario]') LIMIT 1");
		}	
		else
			$mysqli->query("UPDATE usuarios SET email = '$_POST[email]', nombre = '$_POST[nombre]', nivel = '$_POST[nivel]', codigo = '$_POST[codigo]' WHERE(id_usuario = '$_POST[usuario]') LIMIT 1");

		echo '<script type="text/javascript">
			alert("Cambios guardados correctamente");			
			parent.ver_usuarios();			
			parent.Shadowbox.close();			
		</script>		';
		exit();
	}


	$usu = $mysqli->query("SELECT * FROM usuarios WHERE(id_usuario = '$_GET[usuario]') LIMIT 1");
	$u = $usu->fetch_assoc();

?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
<title>Modificar Usuario</title>
<link rel="stylesheet" type="text/css" href="estilos.css">
<style type="text/css">
body
{
	background: white;
	margin: 10px;
}
</style>
</head>

<body>
<div class="cajas">	
<div class="titulos_cajas">Modificar Usuario <?php echo $u['nombre'];?></div>
<form action="usuarios_modificar.php" name="musuarios" method="post">
	<p><label for="nombre">Nombre:</label><input type="text" id="nombre" name="nombre" placeholder="Nombre Completo" autofocus value="<?php echo $u['nombre'];?>"></p>
	<p><label for="email">Correo Electrónico:</label><input type="email" id="email" name="email" placeholder="Correo Electrónico" value="<?php echo $u['email'];?>"></p>
	<p><label for="clave">Clave:</label><input type="password" id="clave" name="clave" placeholder="Deje en blanco para no cambiar"></p>
	<p><label for="nivel">Nivel:</label>
		<select id="nivel" name="nivel">
			<option value="AD" <?php if($u['nivel'] == 'AD') echo 'selected="selected"';?> >Coordinador</option>
			<option value="AN" <?php if($u['nivel'] == 'AN') echo 'selected="selected"';?> >Analistas de Servicios Navieros</option>
			<option value="AO" <?php if($u['nivel'] == 'AO') echo 'selected="selected"';?> >Analista Administrativo de Operaciones</option>
			<option value="OP" <?php if($u['nivel'] == 'OP') echo 'selected="selected"';?> >Operador Portuario</option>
			<option value="CO" <?php if($u['nivel'] == 'CO') echo 'selected="selected"';?> >Consultas</option>
			<option value="FA" <?php if($u['nivel'] == 'FA') echo 'selected="selected"';?> >Facturación</option>
		</select><input type="hidden" name="usuario" id="usuario" value="<?php echo $u['id_usuario'];?>">
	</p>
	<p><label>Código:</label><input type="text" id="codigo" name="codigo" placeholder="Código del Usuario" value="<?php echo $u['codigo'];?>"> 
	</p>
	<p style="text-align:center"><input type="submit" value="Modificar" id="modificar_usuario" name="modificar_usuario"></p>
</form>
</div>

</body>


</html>