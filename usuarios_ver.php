<?php
	include("includes/conexion.php");	
?>
<link rel="stylesheet" type="text/css" href="jsha/shadowbox.css">
	<script type="text/javascript" src="jsha/shadowbox.js"></script>
	<script type="text/javascript">
	  Shadowbox.init();

	function ver_usuarios()
	{
		$("#pantallas").load("usuarios.php");
	}
	</script>

<div class="cajas">
	<div class="titulos_cajas">Usuarios Registrados</div>

					
		<?php
		$datos = $mysqli->query("SELECT * FROM usuarios ORDER BY nombre ASC");	

		if($datos->num_rows > 0)
		{

			echo '<table>
					<tr>
						<td style="width:62%">
						  <strong>Nombre del Usuario</strong>
						</td>
						<td style="width:25%; text-align:center;">
						  <strong>Código</strong>
						</td>						
						<td style="width:25%">
						  <strong>Acciones</strong>
						</td>						
					</tr>	
			';
				$datos->data_seek(0);
				while ($fila = $datos->fetch_assoc()) 
				{					
						if($fila['activo'] == 1)
							$estatus = '<img src="iconos/asemed_listo.png" title="Usuario Activo, Click para Desactivar">';
						else
							$estatus = '<img src="iconos/asemed_eliminar.png" title="Usuario Inactivo; Click para Activar">';				


					echo '<tr class="filas">';
				    echo '<td><a href="usuarios_modificar.php?usuario='. $fila['id_usuario'].'" rel="shadowbox;width=620;height=400" style="color:grey">'. $fila['nombre'].'</a></td>';			
				    echo '<td style="text-align:center">'. $fila['codigo'].'</td>';
				    echo '<td class="acciones"><p><a href="javascript:activar_desactivar('.$fila['id_usuario'].', '.$fila['activo'].');">'.$estatus.'</a></p></td>';
				    echo '</tr>';
				}
			echo '</table>';
		}	
		else
			echo "No hay Usuarios Registrados";

		?>	

	</div>
</div>
