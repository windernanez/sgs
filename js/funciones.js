function IsNumeric(valor)// FORMATEAR FECHA
{
	var log=valor.length; var ok="S";
	for (x=0; x<log; x++)
    {
		v1=valor.substr(x,1);
		v2 = parseInt(v1);
		//Compruebo si es un valor num?rico
		if (isNaN(v2)) { ok= "N";}
    }
    if (ok=="S") {return true;} else {return false; }
}


/*************************************************************
 * Ejemplo de Implementacion para mayusculas y minusculas
 * 'onkeyup'=>'pasarMayusculas(this.value, this.id)',/
 **************************************************************/
function pasarMayusculas(texto, elemento_id)
{
    document.getElementById(elemento_id).value=texto.toUpperCase();
}

   /******************************************
    * Ejemplo de Implementacion Funciones solo letras y solo numeros
    * 'onkeyup'      => 'FormatoRIF(event, this.id, this.value)'
    *
    *******************************************/
   function FormatoRIF(evento, elemento_id, texto)
   {
      var key;
      var letras = ['V','E','P','J','G']; // Letras permitidas
      var patron = /^[VEPJG]{1}-[0-9]{8}-[0-9]{1}$/i; //Patrón del RIF

      texto = texto.toUpperCase();
      longitud = texto.length;

      if(window.event) // IE
      {
         key = evento.keyCode;
      }
      else if(evento.which) // Netscape/Firefox/Opera
      {
         key = evento.which;
      }

      if(key==13 || key==8 )/*para las teclas enter y borrar */
      {
         return true;
      }

      if( (longitud == 1 && letras.indexOf(texto) != -1) || longitud == 10 ) // Concatenar el - después del primer caracter
         return document.getElementById(elemento_id).value=texto + '-';
      else if (longitud == 12) // Al completar el RIF verificar si cumple con el patrón
      {
         if(texto.match(patron))
            return document.getElementById(elemento_id).value= texto;
         else
            return document.getElementById(elemento_id).value=texto.slice(0, -1);
      }//else if
      else if (  texto.length > 2 ) //Verificar que despues de la letra y el guión se ingresen solo números
      {
         if(IsNumeric(texto.substring(2,texto.len)))
            return document.getElementById(elemento_id).value= texto;
         else
           return document.getElementById(elemento_id).value=texto.slice(0, -1);
      }
      else
         return document.getElementById(elemento_id).value=texto.slice(0, -1);
   }//formatoRif

   /******************************************
    * Ejemplo de Implementacion Funciones solo letras y solo numeros
    * 'onKeypress'=>'javascript:return SoloNumeros(event)',
    *
    *******************************************/
function SoloNumeros(evento)
{
 var key;

 if(window.event) // IE
 {
  key = evento.keyCode;
 }
  else if(evento.which) // Netscape/Firefox/Opera
 {
  key = evento.which;
 }
 if(key==13 || key==8)/*para las teclas enter y borrar*/
    {
        return true;
    }
 else if (key < 48 || key > 57)/* para restringir las letras*/
    {
      return false;
    }

 return true;
}


