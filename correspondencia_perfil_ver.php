<?php
	include("includes/conexion.php");	
	session_start();

	$usuario = $_SESSION['nhsgcusuario'];
	$idusuario = $_SESSION['nhsgcidusuario'];
?>

<div class="cajasG">
<div class="titulos_cajas">Reporte por Usuarios</div>

<?php
	if($_POST['tipou'] != "Todos")
		$usuarios = $mysqli->query("SELECT * FROM usuarios WHERE(nivel = '$_POST[tipou]')");
	else
		$usuarios = $mysqli->query("SELECT * FROM usuarios WHERE(nivel = 'OP' OR nivel = 'AN')");

	$asuntos = $mysqli->query("SELECT * FROM comisiones GROUP BY asunto ASC");	

	if(!$asuntos->num_rows)
	{
		echo "<h2>Se debe configurar las comisiones, vaya a Configuración -> Comisiones</h2>";
		exit();
	}		
	//$autoridades = $mysqli->query("SELECT * FROM comisiones GROUP BY  ASC");	

$hayusuarios= 0;

if(!$usuarios->num_rows)
	echo "<h2>Sin usuarios registrados</h2>";
else
	$hayusuarios = 1;

$fechas = date("d/m/y",strtotime($_POST['desde']))." a ".date("d/m/y",strtotime($_POST['hasta']));
 while($u = $usuarios->fetch_assoc())
 {

?>
<h2 style="text-align:left"><?php echo $u['nombre'];?> <span style="font-size:11px;">(<?php echo $fechas; ?>)</span></h2>
<table style="width:100%">
	<tr class="titulos">
		<td>Asunto</td>
		<td>Elaboración</td>
		<td>N° Control</td>
		<td>Tramitación</td>
		<td>N° Control</td>
		<td>Total</td>		
	</tr>

	<?php
	$totale=0;
	$totalt=0;
	$asuntos->data_seek(0);
	while($a = $asuntos->fetch_assoc())
	{

		$etotal = 0;
		$econtrol = "";

		$las_aut = "";
		$autoridades = $mysqli->query("SELECT * FROM comisiones WHERE(asunto = '$a[asunto]')");
		while($aut = $autoridades->fetch_assoc())
		{
			if($las_aut != "")
				$las_aut .= " OR ";
			
			$las_aut .= " autoridad = '".$aut['autoridad']."'";
		}


		$elab = $mysqli->query("SELECT control FROM registros INNER JOIN asuntos ON(asuntos.asunto = registros.asunto) WHERE(elaborada = '$u[id_usuario]' AND registros.asunto = '$a[asunto]' AND ($las_aut) AND (registros.salida >= '$_POST[desde]' AND registros.salida <= '$_POST[hasta]') AND registros.procesado = 1 AND registros.retorno <> '0000-00-00 00:00:00')");
		while($e = $elab->fetch_assoc())
		{
			$etotal++;
			$econtrol .= $e['control']." | ";
		}


		$ttotal = 0;
		$tcontrol = "";
		$tram = $mysqli->query("SELECT control FROM registros INNER JOIN asuntos ON(asuntos.asunto = registros.asunto) WHERE(tramitador = '$u[id_usuario]' AND registros.asunto = '$a[asunto]' AND ($las_aut) AND (registros.salida >= '$_POST[desde]' AND registros.salida <= '$_POST[hasta]') AND registros.procesado = 1 AND registros.retorno <> '0000-00-00 00:00:00')");
		while($t = $tram->fetch_assoc())
		{
			$ttotal++;
			$tcontrol .= $t['control']." | ";
		}

		$totale += $etotal;
		$totalt += $ttotal;

	?>
		<tr>
			<td style="text-align:left"><?php echo $a['asunto'];?></td>
			<td class="centro"><?php echo $etotal;?></td>
			<td class="centro"><?php echo $econtrol;?></td>
			<td class="centro"><?php echo $ttotal;?></td>
			<td class="centro"><?php echo $tcontrol;?></td>			
			<td class="centro"><?php echo $etotal+$ttotal;?> </td>
		</tr>				    
	<?php	    
	}///asuntos		
?>
		<tr>
			<td>TOTALES</td>
			<td class="centro"><?php echo $totale;?></td>
			<td class="centro">-</td>
			<td class="centro"><?php echo $totalt;?></td>
			<td class="centro">-</td>			
			<td class="centro"><?php echo $totale+$totalt;?> </td>
		</tr>
</table>
<div class="clr"></div>
<?php
	}//usuarios
	if($hayusuarios==1)
		echo '<a href="perfil_imprimir.php?desde='.$_POST['desde'].'&hasta='.$_POST['hasta'].'&tipou='.$_POST['tipou'].'" target="_blank">Imprimir</a>';
?>