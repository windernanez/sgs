<?php
//error_reporting(~0);
//ini_set('display_errors',1);
	include("includes/conexion.php");
	include("seguridad.php");
        include ("funciones.class.php");

?>
<link rel="stylesheet" type="text/css" href="jsha/shadowbox.css">
	<script type="text/javascript" src="jsha/shadowbox.js"></script>
	<script type="text/javascript">
	  Shadowbox.init();

	 function marcar_elaboracion(control)
	{
		$("#procesar").load("marcar_salida.php",{control: control});
	}


	function marcar_salida(control, tramitador)
	{
		$("#procesar").load("marcar_salida_final.php",{control: control, tramitador: tramitador});
	}



	 function eliminar_correspondencia(control, obs)
	{
		$("#procesar").load("correspondencia_eliminar_bd.php",{control: control, obs: obs});
	}

	</script>
<div class="cajasG">
    <div class="titulos_cajas">Solicitudes Registradas</div>
    <table style="width:100%">
        <tr class="titulos">
            <td>Nº de Control</td>
            <td>Fecha Sol.</td>
            <td>Línea/Proyecto</td>
            <td>Embarcación</td>
            <td>Viaje</td>
            <td>Asunto de Comunicación</td>
            <td>Autoridad</td>
            <td>Asignada a</td>
        </tr><?php
        if($_POST['filtro'] == "Todas")
            $ver = $mysqli->query("SELECT *, datediff(fecha_s, curdate()) as dias FROM registros WHERE(MONTH(fecha_s) = MONTH(curdate()) ) ORDER BY fecha_s ASC");
        if($_POST['filtro'] == "Pendientes")
            $ver = $mysqli->query("SELECT *, datediff(fecha_s, curdate()) as dias FROM registros WHERE(salida = '0000-00-00 00:00:00') ORDER BY fecha_s ASC");
        if($_POST['filtro'] == "Canceladas")
            $ver = $mysqli->query("SELECT * FROM registros WHERE(MONTH(fecha_s) = MONTH(curdate()) AND procesado = '0' ) ORDER BY fecha_s ASC");
        if($_POST['filtro'] == "Completadas")
            $ver = $mysqli->query("SELECT * FROM registros WHERE(MONTH(fecha_s) = MONTH(curdate()) AND retorno <> '0000-00-00 00:00:00' ) ORDER BY fecha_s ASC");

        if($_POST['filtro'] == 'Todas')
            $tipo ="";
        else
            $tipo = $_POST['filtro'];

        if(!$ver->num_rows)
        {
            echo '<tr>
                    <td colspan="9" style="text-align:center">Sin Solicitudes '.$tipo.' Registradas</td>
                  </tr>
            ';
        }
        else
        {
            while($v = $ver->fetch_assoc())
            {
                $op = $mysqli->query("SELECT nombre FROM usuarios WHERE(id_usuario = '$v[asignada]') LIMIT 1");
                $o = $op->fetch_assoc();
                $operador = $o['nombre'];

                $lipo = $mysqli->query("SELECT nombrel FROM lineas WHERE(id_linea = '$v[lipo]') LIMIT 1");
                $lp = $lipo->fetch_assoc();

                $emb = $mysqli->query("SELECT nombre FROM embarcaciones WHERE(id_embarcacion = '$v[embarcacion]') LIMIT 1");
                $e = $emb->fetch_assoc();

                if(!$v['viaje'])
                    $viaje = "-";
                else
                    $viaje = $v['viaje'];

                ////SEMAFORO
                $alerta="";
                $boton = "";
                if(($tipo == "")||($tipo == "Pendientes"))
                {
                    if(($v['retorno'] != '0000-00-00 00:00:00')&&($v['procesado'] == 1))
                        $alerta = "style=\"background: url('imagenes/verde.png') no-repeat  15%50%\"";
                    elseif(($v['retorno'] == '0000-00-00 00:00:00')&&($v['salida'] != '0000-00-00 00:00:00'))
                    {
                        $alerta = "style=\"background: url('imagenes/transito.png') no-repeat  15%50%\"";
                    }
                    else
                    {
                        if($v['dias'] < 4)
                        {
                            if($v['dias'] < 2)
                                $alerta = "style=\"background: url('imagenes/rojo.png') no-repeat  15%50%\"";
                            elseif($v['dias'] < 4)
                                $alerta = "style=\"background: url('imagenes/amarillo.png') no-repeat  15%50%\"";
                        }
                    }//else
                }//if ?>
                <tr>
                    <td class="centro" <?php if($alerta) echo $alerta;?>><?php echo $v['control'];?> </td>
                    <td class="centro"><?php echo date("d/m/Y",strtotime($v['fecha_s']));?></td>
                    <td><?php echo $lp['nombrel'];?></td>
                    <td><?php echo $e['nombre'];?></td>
                    <td class="centro"><?php echo $viaje;?></td>
                    <td><?php echo $v['asunto'];?></td>
                    <td><?php echo $v['autoridad'];?></td>
                    <td title="<?php echo $operador_sn;?>" class="centro"><?php echo $operador;?></td>
                    <td><?php
    					if(($v['elaborada'] == 0) && ($v['procesado'] == 1) && ($v['salida'] == '0000-00-00 00:00:00'))
        				{ ?>
                            <a href="correspondencia_elaborar.php?control=<?php echo $v['control'];?>" rel="shadowbox;width=400;height=250"><img src="imagenes/elaborar.png" title="Elaborar Solicitud N° <?php echo $v['control'];?>"></a><?php
                        }

                        if(($v['elaborada'] != 0) && ($v['procesado'] == 1))
                        {
                            if($v['salida'] == '0000-00-00 00:00:00')
                            { ?>
                                <a href="correspondencia_marcar_salida.php?control=<?php echo $v['control'];?>" rel="shadowbox;width=400;height=250"><img src="imagenes/salida.png" title="Marcar Salida Solicitud N° <?php echo $v['control'];?>"></a><?php
                            }
                        }//if

						if(($v['retorno'] == '0000-00-00 00:00:00')&& ($v['procesado'] == 1))
						{ ?>
                            <a href="correspondencia_eliminar.php?control=<?php echo $v['control'];?>" rel="shadowbox;width=400;height=250"><img src="imagenes/eliminar.png" title="Eliminar Solicitud N° <?php echo $v['control'];?>"></a><?php
                        }	//si no ha retornado ?>
                    </td>
                </tr><?php
            }//while
        }//else ?>
    </table><?php
    $clientes = $mysqli->query("SELECT * FROM clientes ORDER BY codigo");
    $mostrar_cartelera = false;
    $arr_datos = array();
    if($clientes->num_rows)
    { ?>
      <br><br><br><div class="titulos_cajas">Control de Vigencia de Documentos</div>
        <p style="border: 1px solid grey; padding: 5px;font-size: 14px;" id="los_filtros">
            <img src="imagenes/rojo.png"> Solicitar los Documentos Vigentes  |
            <img src="imagenes/amarillo.png"> Notificar al Cliente Actualización de Documentos |
            <img src="imagenes/naranja.png"> Documento Pendiente por Cargar
        </p>
        <table style="width:100%">
            <tr class="titulos">
                <td>Cod. Cliente</td>
                <td>Cliente</td>
                <td>Documento</td>
                <td>Fec. Vencimiento</td>
                <td>Nº Días</td>
                <td>Contacto</td>
                <td>Email Contacto</td>
                <td>Acciones</td>
            </tr><?php
        while($c = $clientes->fetch_assoc())
        {
            //Buscar documentos del cliente
            //$dc = $mysqli->query("select * from docs_cliente where clientes_id =".(int)$c["id_cliente"]." and fecha_venc != '0000-00-00'");
            $dc = Funciones::DocsVencidos((int)$c["id_cliente"]);
            if(count($dc) > 0)
            {
               while($d = $dc->fetch_assoc())
               {
                  extract($d);
                  //$cal = $mysqli->query("select datediff('".$fecha_venc."', curdate()) as diferencia from docs_cliente where id=".$id);
                  //$x = $cal->fetch_assoc();
                 // if($x["diferencia"] <= 100)
                 // {
                     $mostrar_cartelera=true;
                  //   $doc = $mysqli->query("SELECT * FROM documentos WHERE id_doc=".$documentos_id." LIMIT 1");
                  //   while($d2 = $doc->fetch_assoc())
                  //   {
                       // $ndias = $x["diferencia"];
                        $alerta = NULL;
                        if($diferencia > 0)
                           $alerta = "style=\"background: url('imagenes/amarillo.png') no-repeat  15%50%\"";
                        else
                           $alerta = "style=\"background: url('imagenes/rojo.png') no-repeat  15%50%\""; ?>

                        <tr>
                           <td <?php if ($alerta) echo $alerta; ?>><?php echo $c["codigo"] ?></td>
                           <td><?php echo $c["nombre"] ?></td>
                           <td><?php echo $tipo_doc ?></td>
                           <td><?php echo date_format(date_create($fecha_venc),'d/m/Y') ?></td>
                           <td><font color="red"><b><?php echo $diferencia ?></b></font></td>
                           <td><?php echo $c["contacto"] ?></td>
                           <td><?php echo $c["correo"] ?></td>
                           <td>
                              <img src="imagenes/elaborar.png" onclick="actualizar_doc(<?php echo $id ?>);" style="cursor: pointer;" title="Actualizar documento">
                              &nbsp;&nbsp;
                              <img src="imagenes/correo.png" onclick="enviar_notificacion(<?php echo $id_cli ?>);" style="cursor: pointer;" title="Enviar notificación" id="correo_<?php echo $d["id"] ?>">
                           </td>
                        </tr><?php
                  //   }//while
//                     $arr_datos[$id] = array("codcli" => $cl["codigo"], "nomcli" => $cl["nombre"], "doc" => $d2["tipo_doc"], "fecven" => date_create($fecha_venc), "ndias" => $c["diferencia"],
//                                              "contacto" => $cl["contacto"], "email" => $cl["correo"]);

                //  }//if
               }//while
            }//if

            //Buscar los documentos requeridos que no hayan sido cargados
            $falt = $mysqli->query("select * from documentos where id_doc not in (select documentos_id from docs_cliente where clientes_id=".(int)$c["id_cliente"].") and requerido='1'");
            $alerta = "style=\"background: url('imagenes/naranja.png') no-repeat  15%50%\"";
            if($falt->num_rows)
            {
               while($df = $falt->fetch_assoc())
               { ?>
                  <tr>
                     <td <?php if ($alerta) echo $alerta; ?>><?php echo $c["codigo"] ?></td>
                     <td><?php echo $c["nombre"] ?></td>
                     <td><?php echo $df["tipo_doc"] ?></td>
                     <td><?php echo '---' ?></td>
                     <td><font color="red"><b><?php echo '---' ?></b></font></td>
                     <td><?php echo $c["contacto"] ?></td>
                     <td><?php echo $c["correo"] ?></td>
                     <td>
                        <!--<img src="imagenes/elaborar.png" onclick="actualizar_doc(<?php echo $d["id"] ?>);" style="cursor: pointer;" title="Actualizar documento">-->
<!--                        &nbsp;&nbsp;
                        <img src="imagenes/correo.png" onclick="enviar_notificacion(<?php echo $d["id"] ?>);" style="cursor: pointer;" title="Enviar notificación" id="correo_<?php echo $d["id"] ?>">-->
                     </td>
                  </tr><?php

               }
            }
            /*
            */
        }//while ?>
        </table>
        <?php
    }//if

    /*if(count($arr_datos) > 0)
    { ?>
        <br><br><br><div class="titulos_cajas">Control de Vigencia de Documentos</div>
        <p style="border: 1px solid grey; padding: 5px;font-size: 14px;" id="los_filtros">
            <img src="imagenes/rojo.png"> Solicitar los Documentos Vigentes  | <img src="imagenes/amarillo.png"> Notificar al Cliente Actualización de Documentos
        </p>
        <table style="width:100%">
            <tr class="titulos">
                <td>Cod. Cliente</td>
                <td>Cliente</td>
                <td>Documento</td>
                <td>Fec. Vencimiento</td>
                <td>Nº Días</td>
                <td>Contacto</td>
                <td>Email Contacto</td>
                <td>Acciones</td>
            </tr><?php
            foreach($arr_datos as $i => $valores)
            {
                $alerta = NULL;
                if($valores["ndias"] > 0)
                    $alerta = "style=\"background: url('imagenes/amarillo.png') no-repeat  15%50%\"";
                else
                    $alerta = "style=\"background: url('imagenes/rojo.png') no-repeat  15%50%\""; ?>
                <tr>
                    <td <?php if ($alerta) echo $alerta; ?>><?php echo $valores["codcli"] ?></td>
                    <td><?php echo $valores["nomcli"] ?></td>
                    <td><?php echo $valores["doc"] ?></td>
                    <td><?php echo date_format($valores["fecven"],'d/m/Y') ?></td>
                    <td><font color="red"><b><?php echo $valores["ndias"] ?></b></font></td>
                    <td><?php echo $valores["contacto"] ?></td>
                    <td><?php echo $valores["email"] ?></td>
                    <td>
                       <img src="imagenes/elaborar.png" onclick="actualizar_doc(<?php echo $i ?>);" style="cursor: pointer;" title="Actualizar documento">
                        &nbsp;&nbsp;
                       <img src="imagenes/correo.png" onclick="enviar_notificacion(<?php echo $i ?>);" style="cursor: pointer;" title="Enviar notificación" id="correo_<?php echo $i ?>">
                    </td>
                </tr><?php
            } ?>
        </table>
        <div id="reporte_ver" style="text-align:center"></div><?php
    }*/ ?>
        <div id="reporte_ver" style="text-align:center"></div><br><br>
</div>
<script>
    function actualizar_doc(id)
    {
        $("#pantallas").load("actualizar_doc.php", {iddoc : id});
    }

    function enviar_notificacion(id)
    {
        if(confirm('¿Enviar notificacion por correo electrónico al Cliente?'))
        {
            $("#correo_"+id).hide();
            $("#reporte_ver").html('<img src="loading.gif" height="40"><br>Enviando Notificación, por favor espere...');
            $("#reporte_ver").load("enviar_notificacion.php", {idcli : id});
        }
        else
        {
            $("#correo_"+id).show();
            return false;
        }
    }
</script>
