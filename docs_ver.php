<?php
	include("includes/conexion.php");
?>
<div class="cajas">
   <div class="titulos_cajas">Documentos Registrados</div><?php
   $datos = $mysqli->query("SELECT * FROM documentos ORDER BY tipo_doc ASC");

   if($datos && $datos->num_rows > 0)
   { ?>

      <table style="width: 100%">
         <tr>
            <td style="width: 50%">Tipo de Documento</td>
            <td style="width: 20%">Abreviatura</td>
            <td style="width: 10%">¿Doc. Vence?</td>
            <td style="width: 10%">Activo</td>
            <td style="width: 10%">Acciones</td>
         </tr><?php

         $datos->data_seek(0);
         while ($fila = $datos->fetch_assoc())
         {
            $estatus = '<img src="iconos/asemed_eliminar.png" title="Eliminar '.$fila["tipo_doc"].'">'; ?>
            <tr class="filas">
               <td><?php echo $fila['tipo_doc'] ?></td>
               <td><?php echo $fila['abr_doc'] ?></td>
               <td><?php echo $fila['fvenc'] ? 'SI' : 'NO' ?></td>
               <td><?php echo $fila['activo'] ? 'SI' : 'NO' ?></td>
               <td class="acciones"><p><a href="javascript:documentos_borrar('<?php echo $fila['id_doc'] ?>');"><?php echo $estatus ?></a></p></td>
            </tr><?php
         }
      echo '</table>';
   }//if
   else
      echo "<center>No hay Documentos Registrados</center>"; ?>
</div>
