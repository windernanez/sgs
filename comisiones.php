<?php
	session_start();
	include("includes/conexion.php");	
?>
<div class="cajas">
<div class="titulos_cajas">Configurar Comisiones</div>
<p><label for="asunto">Asunto:</label><select id="asunto">		
		<?php
			$em = $mysqli->query("SELECT * FROM asuntos ORDER BY asunto ASC");			
			while($e = $em->fetch_assoc())
			{
				echo '<option value="'.$e['asunto'].'">'.$e['asunto'].'</option>';
			}
		?>
	</select>
</p>


<p><label for="autoridad">Autoridad:</label><select id="autoridad">		
		<?php
			$em = $mysqli->query("SELECT * FROM autoridades ORDER BY nombre ASC");
			while($e = $em->fetch_assoc())
			{
				echo '<option value="'.$e['nombre'].'">'.$e['nombre'].'</option>';
			}
		?>
	</select>
</p>

<p style="text-align:center"><input type="button" value="Guardar" id="registrar_comision"></p>
</div>
<p id="pantallas_ver"></p>
<script>
	$( "#registrar_comision" ).click(function() {
	  		comision_registrar();
		});	
	$("#pantallas_ver").load("comisiones_ver.php");
</script>