<?php
include("includes/conexion.php");
include("seguridad.php");
$codigo = isset($_GET['codigo']) ? $_GET['codigo'] : "";
$abrdoc = isset($_GET['abrdoc']) ? $_GET['abrdoc'] : $_POST['abrdoc'];
$usuario = $_SESSION['nhsgcusuario'];
$idusuario = $_SESSION['nhsgcidusuario'];
date_default_timezone_set('America/Caracas');
if(!$codigo)
 	$codigo = isset($_POST['codigo']) ? $_POST['codigo'] : NULL;

if(isset($_POST['enviar']) && $_POST['enviar'])
{
   $extension = explode(".",$_FILES['adjunto']['name']);
   $final = count($extension)-1;
   if($extension[$final] != "pdf")
   { ?>
      <script>
         alert('Debe adjuntar documentos en formato PDF');
         parent.Shadowbox.close();
      </script><?php
      die();
   }
   $ver = $mysqli->query("select * from clientes WHERE(codigo = '$codigo') LIMIT 1");

   if($ver->num_rows)
   {
      $v = $ver->fetch_assoc();
      extract($v);

      $carpeta_cliente = "adjuntos/EXPEDIENTES DEL CLIENTE/".$codigo." --- ".$nombre;
      $carpcliente = substr($carpeta_cliente, 33);

      //SE CREA LA CARPETA
      if(!is_dir($carpeta_cliente)){
         mkdir($carpeta_cliente, 0777, true);
      }

      $docs = $mysqli->query("select * from documentos WHERE(abr_doc = '$abrdoc') LIMIT 1");
      if($docs->num_rows)
      {
         $x = $docs->fetch_assoc();
         extract($x);
      }

      //SE CREA EL NOMBRE DEL ARCHIVO
      $nombre ="";
      $nombre_archivo = $tipo_doc.".".$extension[$final];

      $cli = $mysqli->query("SELECT * FROM clientes WHERE(codigo = '$codigo') LIMIT 1");
      $c  = $cli->fetch_assoc();
      $id_cliente = $c['id_cliente'];
   }

   //Si el archivo existe se mueve a la carpeta historico con la fecha en la que se reemplazó
   if (file_exists($carpeta_cliente."/".$nombre_archivo))
   {
      $fecha = date('dmy_His');
      $carpeta2 = "adjuntos/HISTORICOS";

      //SE CREA LA CARPETA
      if(!is_dir($carpeta2)){
         mkdir($carpeta2, 0777, true);
         chmod($carpeta2,  0777);
      }

      //Se crea la carpeta del cliente
      $carpetac = $carpeta2."/".$carpcliente;

      if(!is_dir($carpetac)){
         mkdir($carpetac, 0777, true);
         chmod($carpetac,  0777);
      }

      $ruta_reemplazo = $carpetac."/".$fecha."_".$idusuario."_".$nombre_archivo;
      copy($carpeta_cliente."/".$nombre_archivo, $ruta_reemplazo);
      chmod($ruta_reemplazo,  0777);
//      rename($carpeta_cliente."/".$nombre_archivo, $ruta_reemplazo);
   }

   if(move_uploaded_file($_FILES['adjunto']['tmp_name'],$carpeta_cliente."/".$nombre_archivo))
   { ?>
	<script type="text/javascript">
//            alert("Archivo Subido Correctamente");
            parent.ver_doc_adjunto_cliente('<?php echo $carpeta_cliente."/".$nombre_archivo;?>', '<?php echo strtolower($_POST['abrdoc']) ?>');
            parent.Shadowbox.close();
	</script>
<?php
//	}
	/*else
	{
		include("includes/conexion.php");
		$mysqli->query("UPDATE registros SET soporte = '$nombre_archivo' WHERE(control = '$codigo') LIMIT 1");
		?>

	<script type="text/javascript">
				alert("Copia Guardada Correctamente");
				parent.correspondencia_adjuntob('<?php echo $nombre_archivo;?>');
				parent.Shadowbox.close();
	</script>

		<?php

	}*/

   }//si subió la imagen
  else
   $msj = '<strong>El Archivo NO fue guardado. Ocurrió un error</strong>';
}


?>
<!DOCTYPE html>
<html>
<head>
<title>Guardando Copia de Correspondencia</title>
<style type="text/css">
body
{
	background: white;
	margin: 10px;
	font-size: 16px;
	font-family: Arial;
}
</style>
<script type="text/javascript" src="js/jquery-1.11.0.min.js"></script>
<script>
function mostrar_mensaje()
{
$("#boton").hide();
$("#mensaje").html("Subiendo Archivo, por favor espere...");
}
</script>

</head>
<body>

   <form action="adjuntar_doc_cliente.php" method="post" enctype="multipart/form-data" name="noticias" id="noticias">
    <table width="100%" border="0">
      <tr>
        <td width="40%" style="text-align:center"><strong>Copia Digitalizada</strong></td>
        <td width="60%" style="text-align:center">
            <input name="adjunto" type="file" id="adjunto" title="Seleccione el Archivo"/>
            <input name="codigo" type="hidden" id="codigo" value="<?php echo $codigo; ?>" />
            <input name="abrdoc" type="hidden" id="abrdoc" value="<?php echo strtoupper($abrdoc); ?>" />
            <span id="boton"><input name="enviar" type="submit" class="bv10" id="enviar"  value="Subir" onclick="mostrar_mensaje();" /></span>
            <span id="mensaje"></span>
        </td>
      </tr>
    </table>
  </form>

</form>
</body>
</html>