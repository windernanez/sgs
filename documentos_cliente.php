<script type="text/javascript" src="datepicker/js/jquery.datepick-es.js"></script>
<?php
include("includes/conexion.php");
include("seguridad.php");
?>
<link rel="stylesheet" type="text/css" href="jsha/shadowbox.css">
<script type="text/javascript" src="jsha/shadowbox.js"></script>
<script type="text/javascript">
//  Shadowbox.init();
</script>

<?php
$usuario = $_SESSION['nhsgcusuario'];
$idusuario = $_SESSION['nhsgcidusuario'];
?>

<div class="cajas">
   <div class="titulos_cajas">Documentación del Cliente</div>
   <p><label for="codcli">Cod. Profit:</label><input type="text" id="codcli" placeholder="Código Profit del Cliente" autofocus onKeyup="javascript:return pasarMayusculas(this.value, this.id)"></p>
   <p><label>RIF: </label><span class="cajitas" id="rif" disabled="disabled">-</span></p>
   <p><label>Nombre: </label><span class="cajitas" id="nombre" disabled="disabled">-</span></p>
   <p><label>Céd. Otorgante: </label><span class="cajitas" id="cedoto" disabled="disabled">-</span></p>
   <p><label>RIF Otorgante: </label><span class="cajitas" id="rifoto" disabled="disabled">-</span></p>
   <p><label>Nombre Otorgante: </label><span class="cajitas" id="nomoto" disabled="disabled">-</span></p>

   <div id="ver_docs"></div>
   <!--<div class="titulos_cajas" id="control_docs">Control de Vigencia y Adjuntos</div>--><?php
   /*$li = $mysqli->query("SELECT * FROM documentos WHERE(activo = 1) ORDER BY tipo_doc ASC");
   if(!$li->num_rows)
      echo '
         <script>
            alert("Debe registrar Documentos para continuar")
            $("#pantallas").load("documentos.php");
         </script>';

   while($l = $li->fetch_assoc())
   { //echo $l['id_doc'].' > '.$l['tipo_doc'].'<br>';
      ?>
      <p>
         <label><b><?php echo ($l['fvenc'] ? 'F. Venc. ' : '').$l['tipo_doc'] ?>:</b></label><br><?php
         if($l['fvenc']): ?>
         <input type="text" maxlength="0" id="fvenc_<?php echo strtolower($l['abr_doc']) ?>" style="width: 150px;"><?php
         endif; ?>
         <a style="display: none;" href="adjuntar_doc_cliente.php?control=x" rel="shadowbox;width=450;height=100" id="<?php echo 'link_'.strtolower($l['abr_doc']) ?>">Adjuntar Archivo</a> |
         <span id="ver_<?php echo strtolower($l['abr_doc']) ?>">Sin Archivo</span>
         <input type="hidden" id="valoradj_<?php echo strtolower($l['abr_doc']) ?>" value="">
      </p><hr class="cajas"><?php
   }
?>
<p style="text-align:center"><input type="button" value="Guardar" id="guardar_docs_cli"></p>
</div>
<p id="pantallas_ver"></p>
<div class="clr"></div>*/?>
<script type="text/javascript">
   $( "#codcli" ).blur(function() {
         var codcli = $('#codcli').val();

         if(codcli)
            $("#ver_docs").load("buscar_cliente.php", {codcli: codcli});

         $("[id^=link]").each(function(){
            var codigo = $("#codcli").val();
            var abrdoc = (this.id).substr(5);
//            var valor  = $("#"+abrdoc).val();

            $(this).attr("href", "adjuntar_doc_cliente.php?codigo=" + codigo + "&abrdoc=" + abrdoc);
            $(this).show();
         });
         Shadowbox.init();

         $("[id^=fvenc_]").each(function(){
            $("#"+this.id).datepick({dateFormat: 'yyyy-mm-dd'});
         });
	});

/*   $( "#guardar_docs_cli" ).click(function() {
      if($("#codcli").val() === '')
      {
         alert('Debe indicar un cliente');
         $("#codcli").focus();
         return false;
      }
      procesar_docs_cli(0);
   });*/
</script>