<?php
include("includes/conexion.php");
include ("funciones.class.php");
session_start();
error_reporting(~0);
ini_set('display_errors',1);
$usuario = $_SESSION['nhsgcusuario'];
$idusuario = $_SESSION['nhsgcidusuario'];
$facturado = "";
?>

<div class="cajasG"><?php
   $cli = Funciones::buscarCliente("codigo",$_POST["codcli"]);

   if(!$cli->num_rows)
   { ?>
      <script>
         alert('No se encontraron registros');
         $("#pantallas").load("expediente_cliente.php");
      </script><?php
   }//if
   else
   {
      $cl = $cli->fetch_assoc();
      $docs = $mysqli->query("SELECT * FROM docs_cliente WHERE clientes_id=".$cl["id_cliente"]); ?>
      <div class="titulos_cajas">Expediente del Cliente</div>
      <table style="width:100%">
         <tr class="titulos">
            <td>Código</td>
            <td>RIF</td>
            <td>Nombre</td>
            <td>Otorgante</td>
            <td>Cédula Otorgante</td>
            <td>RIF Otorgante</td>
            <td>Contacto</td>
            <td>Correo Contacto</td>
         </tr>

         <tr>
            <td><?php echo $cl["codigo"] ?></td>
            <td><?php echo $cl["rif"] ?></td>
            <td><?php echo $cl["nombre"] ?></td>
            <td><?php echo $cl["nombre_otorgante"] ?></td>
            <td><?php echo $cl["cedula_otorgante"] ?></td>
            <td><?php echo $cl["rif_otorgante"] ?></td>
            <td><?php echo $cl["contacto"] ?></td>
            <td><?php echo $cl["correo"] ?></td>
         </tr><?php

         if($docs->num_rows)
         { ?>
            <tr>
               <td class="titulos_cajas" colspan="8">Documentos Consignados</td>
            </tr>

            <tr class="titulos">
               <td colspan="4">Nombre del Documento</td>
               <td colspan="2">Fecha Vencimiento</td>
               <td>Nº Días</td>
               <td colspan="2">Acciones</td>
            </tr><?php
            while($d = $docs->fetch_assoc())
            {
               Funciones::BuscarDatos("SELECT * FROM documentos WHERE id_doc=".$d["documentos_id"]." LIMIT 1", $documentos);
               $documentos = $documentos->fetch_assoc();
               if(($d["fecha_venc"] != '0000-00-00'))
               {
                  $fecven = date_create($d["fecha_venc"]);
                  $cal = $mysqli->query("select datediff('".$d["fecha_venc"]."', curdate()) as diferencia");
                  $c = $cal->fetch_assoc();
                  $ndias = $c["diferencia"];
               }
               else
               {
                  $fecven = 'N/A';
                  $ndias = '--';
               }

               ?>
               <tr>
                  <td colspan="4"><?php echo $documentos["tipo_doc"] ?></td>
                  <td colspan="2"><?php echo $fecven != 'N/A' ? date_format($fecven,'d/m/Y') : $fecven ?></td>
                  <td><?php echo $ndias ?></td>
                  <td>
                     <a href="<?php echo $d["ruta"] ?>" id="<?php echo strtolower($documentos["abr_doc"]) ?>" target="_blank">
                        <img src="imagenes/soporte.png" style="cursor: pointer;" title="Ver Documento"/>
                     </a>
                  </td>
               </tr><?php
            }//while
         }//if
         else
         {
            echo '<tr><td colspan="8"><img src="imagenes/warning.png" /><br>'
            . '<font color="blue"><b>El cliente no ha consignado ninguna documentación</b></font></td></tr>';
         }
?>
      </table>
      <!--<p style="text-align:center"><input type="button" value="Guardar" id="guardar_docs_cli"></p>--><?php
   } ?>
</div>
<p style="text-align:center"><a href="imprimir_expediente.php?cod=<?php echo $_POST["codcli"] ?>" target="blank"><img src="imagenes/pdf.png" id="imprimir_exps"><br><b>Descargar Expediente</b></a></p>
