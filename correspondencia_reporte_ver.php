<?php
	include("includes/conexion.php");	
	session_start();

	$usuario = $_SESSION['nhsgcusuario'];
	$idusuario = $_SESSION['nhsgcidusuario'];
	$facturado = "";	
?>

<div class="cajasG">
<div class="titulos_cajas">Solicitudes Registradas</div>
<table style="width:100%">
	<tr class="titulos">
		<td>Nº de Control</td>
		<td>Línea/Proyecto</td>
		<td>Embarcación</td>
		<td>Viaje</td>
		<td>Asunto de Comunicación</td>
		<td>Autoridad</td>
		<td>Salida</td>
		<td>Elaborador</td>
		<td>Retorno</td>
		<td>Tramitador</td>
		<td>Observaciones</td>
		<td></td>		
	</tr>

<?php

	
	
	if(!$_POST['desde'])
		$ver = $mysqli->query("SELECT * FROM registros WHERE(salida <> '0000-00-00 00:00:00') ORDER BY id_registro DESC");
	else
	{

		if(!$_POST['xcontrol'])
		{
			if(($_POST['embarcacion'])&&( $_POST['embarcacion'] != "Todos"))
				$embarcacion = " AND (embarcacion = ".$_POST['embarcacion'].") ";
			else
				$embarcacion = "";		

			if(($_POST['linea'])&&($_POST['linea'] != "Todos"))
				$linea = " AND (lipo = ".$_POST['linea'].") ";
			else
				$linea = "";		

			if(($_POST['asunto'])&&($_POST['asunto'] != "Todos"))
				$asunto = " AND (asunto = '".$_POST['asunto']."') ";
			else
				$asunto = "";		
				
			if(($_POST['autoridad'])&&($_POST['autoridad'] != "Todas"))
				$autoridad = " AND (autoridad = '".$_POST['autoridad']."') ";
			else
				$autoridad = "";		

			$sql = "SELECT * FROM registros WHERE ( (DATE( salida ) >=  '".$_POST['desde']."') AND  (DATE( salida ) <=  '".$_POST['hasta']."') ".$linea." ".$embarcacion." ".$asunto." ".$autoridad.") ORDER BY id_registro DESC";		
		}//si no es por control
		else
		{

			$control = explode(",", $_POST['xcontrol']);
			if(count($control) == 0)
			 $query = " control = '".$_POST['xcontrol']."' ";	
			else
			{

			$query = "";
			for($i=0;$i<count($control);$i++)
			{
				$query .= " control = '".$control[$i]."' ";
				if(($i+1)<count($control))
				{
					$query .= " OR ";
				}
			}

			}

			$sql = "SELECT * FROM registros WHERE (".$query.") ";					
		}
		//echo "dddd ".$sql;
		$ver = $mysqli->query($sql);		
	}
		

	if(!$ver->num_rows)
	{		
		echo '<tr>
				<td colspan="12" style="text-align:center">Sin Solicitudes Registradas que cumpla con el criterio indicado</td>
			  </tr>	
		';
	}
	else
	{
		while($v = $ver->fetch_assoc())
		{


		if($v['salida'] != '0000-00-00 00:00:00')
		{
			$op = $mysqli->query("SELECT nombre FROM usuarios WHERE(id_usuario = '$v[elaborada]') LIMIT 1");
			$o = $op->fetch_assoc();
			$salida = date("d/m/Y h:i", strtotime($v['salida']));		
		 	$operador_sn = $o['nombre'];
		 	$operador_s = explode(" ",$o['nombre']);
		 	$operador_s = substr($operador_s[0], 0, 1).substr($operador_s[1], 0, 1);
	 	}
	 	else
	 	{
	 		$op = $mysqli->query("SELECT nombre FROM usuarios WHERE(id_usuario = '$v[asignada]') LIMIT 1");
			$o = $op->fetch_assoc();
			$salida = "Pendiente";		
		 	$operador_sn = $o['nombre'];
		 	$operador_s = explode(" ",$o['nombre']);
		 	$operador_s = substr($operador_s[0], 0, 1).substr($operador_s[1], 0, 1)." [A]";	
	 	}

		$lipo = $mysqli->query("SELECT nombrel FROM lineas WHERE(id_linea = '$v[lipo]') LIMIT 1");
		$lp = $lipo->fetch_assoc();

		$emb = $mysqli->query("SELECT nombre FROM embarcaciones WHERE(id_embarcacion = '$v[embarcacion]') LIMIT 1");
		$e = $emb->fetch_assoc();

		if(!$v['viaje'])
			$viaje = "-";
		else
			$viaje = $v['viaje'];

		if($v['retorno'] != '0000-00-00 00:00:00')
		{
			$op = $mysqli->query("SELECT nombre FROM usuarios WHERE(id_usuario = '$v[tramitador]') LIMIT 1");
			$o = $op->fetch_assoc();
		 	$retorno = date("d/m/Y h:i", strtotime($v['retorno']));
		 	$operador_rn = $o['nombre'];
		 	$operador_r = explode(" ",$o['nombre']);
		 	$operador_r = substr($operador_r[0], 0, 1).substr($operador_r[1], 0, 1);

		}
		else
		{
			if($salida != "Pendiente")
		 		$retorno = "Por retornar";
		 	else
		 		$retorno = "-";

		 $operador_r = "-";
		} 		

		$email = "";
		if($v['soporte'])
	    {
	    		$soporte = '<a href="adjuntos/'.$v['soporte'].'" target="_blank"><img src="imagenes/soporte.png" alt="Ver Soporte" title="Ver Soporte"></a>';
	    		//$email = '<a href="#"><img src="imagenes/email.png" title="Enviar por Email"></a>';
	    }
	    else
	    	$soporte = '<img src="imagenes/sin_soporte.png" alt="Sin Soporte" title="Sin Soporte">';


		if($v['soportemail'])	    
	    	$email = '<a href="adjuntosmail/'.$v['soportemail'].'"><img src="imagenes/email.png" title="Ver Email"></a>';	    
	    else
	    	$email = '<img src="imagenes/sinemail.png" alt="Sin Email" title="Sin Email">';


	    $clase ="";

	    if(($retorno != "Por retornar")&&($v['procesado'] == 1))
	    {
			
	    	if($nivel == "FA")
	    	{

				if($v['facturado'] == '0000-00-00 00:00:00')
			    	$facturado = '<a href="javascript:facturar('.$v['control'].');"><img src="imagenes/facturar.png" alt="Marcar como Facturado" title="Marcar como Facturado"></a>';
			    else
			    { 
			    	$fecha = date("d/m/Y h:i", strtotime($v['facturado']));
			    	$facturado = '<img src="imagenes/facturado.png" alt="Facturado" title="Facturado el '.$fecha.'" title="Facturado" title="Facturado el '.$fecha.'">';
			    }

		    }
		    
		}
		else
		{
			if($v['procesado'] == 0)
			{
				$clase = 'style="background:#FF8888"';
				$retorno = "-";
			}
				

			$facturado ="";
			$soporte ="";
		}

	?>
		<tr <?php echo $clase;?> >
			<td class="centro"><?php echo $v['control'];?></td>
			<td><?php echo $lp['nombrel'];?></td>
			<td><?php echo $e['nombre'];?></td>
			<td class="centro"><?php echo $viaje;?></td>
			<td><?php echo $v['asunto'];?></td>
			<td><?php echo $v['autoridad'];?></td>
			<td><?php echo $salida;?></td>
			<td title="<?php echo $operador_sn;?>" class="centro"><?php echo $operador_s;?></td>
			<td><?php echo $retorno;?></td>
			<td title="<?php echo $operador_sn;?>" class="centro"><?php echo $operador_r;?></td>
			<td><?php echo $v['observaciones'];?></td>
			<td class="centro"><?php echo $soporte." ".$facturado." ".$email;?> </td>		
		</tr>				    
	<?php	    
	    }

	  
	
	}
?>
</table>