<?php
	include("seguridad.php");
	$nivel = $_SESSION['nhsgcnivel'];

	include("includes/conexion.php");	
	
	$ver = $mysqli->query("SELECT * FROM registros WHERE ('$_POST[control]' = control) LIMIT 1");

	if(!$ver->num_rows)
	{		
?>
	<link rel="stylesheet" type="text/css" href="jsha/shadowbox.css">
	<script type="text/javascript" src="jsha/shadowbox.js"></script>
	<script type="text/javascript">
	  Shadowbox.init();
	</script>

	<script>				
	    $("#controlb").val("");					
	    $("#linea").html("-");					
	    $("#embarcacion").html("-");					
	    $("#viaje").html("-");						    
	    $("#asunto").html("-");				
	    $("#autoridad").html("-");						    		
	    $("#hora_s").html("-");						    
	    $("#operador_s").html("-");						    
	    $("#hora_r").html("-");						    
	    $("#operador_r").html("-");						    
	    $("#obs").html("-");
	    $("#soporte").html("-");
	    $("#soportemail").html("-");
	    $("#soportextra").html("-");
		$("#controlb").focus();		
		alert('Número de Control NO Registrado');
	</script>
<?php	
	}
	else
	{
		$v = $ver->fetch_assoc();

		if($v['salida'] != '0000-00-00 00:00:00')
		{
		$op = $mysqli->query("SELECT nombre FROM usuarios WHERE(id_usuario = '$v[usuario_s]') LIMIT 1");
		$o = $op->fetch_assoc();
		$salida = date("d/m/Y h:i", strtotime($v['salida']));
		$operador_s = $o['nombre'];
		}
		else
		{
			$op = $mysqli->query("SELECT nombre FROM usuarios WHERE(id_usuario = '$v[asignada]') LIMIT 1");
			$o = $op->fetch_assoc();		
			$salida = "Pendiente";
			$operador_s = $o['nombre']." [Asignado]";
		}

		$lipo = $mysqli->query("SELECT nombrel FROM lineas WHERE(id_linea = '$v[lipo]') LIMIT 1");
		$lp = $lipo->fetch_assoc();

		$emb = $mysqli->query("SELECT nombre FROM embarcaciones WHERE(id_embarcacion = '$v[embarcacion]') LIMIT 1");
		$e = $emb->fetch_assoc();

		if(!$v['viaje'])
			$viaje = "-";
		else
			$viaje = $v['viaje'];

		if($v['retorno'] != '0000-00-00 00:00:00')
		{
			$op = $mysqli->query("SELECT nombre FROM usuarios WHERE(id_usuario = '$v[usuario_r]') LIMIT 1");
			$o = $op->fetch_assoc();
		 	$retorno = date("d/m/Y h:i", strtotime($v['retorno']));
		 	$operador_r = $o['nombre'];
		}
		else
		{
			if($salida == "Pendiente")
				$retorno = "-";
			else
				$retorno = "Por retornar";		 		
			
			$operador_r = "-";				 
		} 		

		if($v['observaciones'])
			$observaciones = $v['observaciones'];
		else
			$observaciones = "-";

?>
	<script>				
	    $("#linea").html("<?php echo $lp['nombrel'];?>");					
	    $("#embarcacion").html("<?php echo $e['nombre'];?>");					
	    $("#viaje").html("<?php echo $viaje;?>");						    
	    $("#asunto").html("<?php echo $v['asunto'];?>");				
	    $("#autoridad").html("<?php echo $v['autoridad'];?>");						    		
	    $("#hora_s").html("<?php echo $salida;?>");						    
	    $("#operador_s").html("<?php echo $operador_s;?>");						    
	    $("#hora_r").html("<?php echo $retorno;?>");						    
	    $("#operador_r").html("<?php echo $operador_r;?>");						    
	    $("#obs").html("<?php echo $observaciones;?>");						    
	    <?php
		
	    if($nivel == "AD")
	    {

	    ?>
	    	$("#editar").html("<a href='javascript:editar_solicitud(<?php echo $_POST['control'];?>)'><img src='imagenes/editar_solicitud.png'></a>")
	    <?php
	    }///si es administrador

	    if($salida == "Pendiente")
	    {
	    ?>	
	    	$("#soporte").html("-");
	    
	    <?php
		}
	    else
	    {

		    if($v['soporte'])
		    {
		?>
		    	$("#soporte").html('<a href="adjuntos/<?php echo $v['soporte']; ?>" target="_blank">Ver Adjunto</a>');
		<?php
		    }	
		    else
		    {
	    ?>
		    	$("#soporte").html('<a href="javascript:adjuntar_copia(\'<?php echo $v[control];?>\')" >Adjuntar Copia</a>');

	    <?php
		    }


		    if($v['soportemail'])
		    {
		?>
		    	$("#soportemail").html('<a href="adjuntosmail/<?php echo $v['soporte']; ?>" target="_blank">Ver Email</a>');
		<?php
		    }	
		    else
		    {
	    ?>
		    	$("#soportemail").html('<a href="javascript:adjuntar_copiamail(\'<?php echo $v[control];?>\')" >Adjuntar Email</a>');

	    <?php
		    }


		    if($v['soportextra'])
		    {
		?>
		    	$("#soportextra").html('<a href="adjuntosextra/<?php echo $v['soportextra']; ?>" target="_blank">Ver Archivo</a>');
		<?php
		    }	
		    else
		    {
	    ?>
		    	$("#soportextra").html('<a href="javascript:adjuntar_copiaextra(\'<?php echo $v[control];?>\')" >Adjuntar Archivo</a>');

	    <?php
		    }

		}
	    ?>
	</script>

<?php
		
	}
?>