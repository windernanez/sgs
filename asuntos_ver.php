<?php
	include("includes/conexion.php");	
?>
<div class="cajas">
	<div class="titulos_cajas">Asuntos de Comunicación Registrados</div>

					
		<?php
		$datos = $mysqli->query("SELECT * FROM asuntos ORDER BY asunto ASC");	

		if($datos->num_rows > 0)
		{

			echo '<table style="width:100%">
					<tr>
						<td style="width:68%">
						  <strong>Titulo del Asunto</strong>
						</td>
						<td style="width:30%">
						  <strong>Acciones</strong>
						</td>						
					</tr>	
			';
				$datos->data_seek(0);
				while ($fila = $datos->fetch_assoc()) 
				{					
							$estatus = '<img src="iconos/asemed_eliminar.png" title="Eliminar '.$fila["asunto"].'">';				


						
								
					echo '<tr class="filas">';
				    echo '<td>'. $fila['asunto'].'</td>';							    
				    echo '<td class="acciones"><p><a href="javascript:asuntos_borrar('.$fila['id_asunto'].');">'.$estatus.'</a></p></td>';
				    echo '</tr>';
				}
			echo '</table>';
		}	
		else
			echo "No hay Asuntos Registrados";

		?>	

	</div>
</div>
