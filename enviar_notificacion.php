<?php
include("includes/conexion.php");
require_once("sfPHPMailer/vendor/class.phpmailer.php");
require_once ("sfPHPMailer/vendor/class.smtp.php");
require_once("funciones.class.php");
//error_reporting(~0);
//ini_set('display_errors',1);
$id_cliente = $_POST["idcli"];
$docs = Funciones::DocsVencidos((int)$id_cliente);
if($docs->num_rows)
{
    $cli = Funciones::buscarCliente('id_cliente', (int)$id_cliente);
    $cli = $cli->fetch_assoc();
    if($cli)
    {
        $body = "<p>Estimado Cliente <b>".$cli["nombre"]."</b>:<br><br>La presente es para notificarle que nuestros registros indican que posee los siguientes documentos vencidos o próximos a vencerse: <b>";
        $body .= "<table>";
        while($dv = $docs->fetch_assoc())
        {
           $fvenci = date_format(date_create($dv["fecha_venc"]),'d/m/Y');
           $body .= "<tr><td><ul><li><b>".$dv["tipo_doc"]."</b></li></ul></td><td><b>&nbsp;&nbsp;"."FECHA VENCIMIENTO: ".$fvenci."</b></td></tr>";
        }
        $body .= "</table><br><br>";

        $body .= "</b>Para mantener su documentación al día responda a éste correo electrónico enviando una copia escaneada del documento en formato PDF."
        . "Si tiene alguna duda puede ponerse en contacto con nosotros por esta vía.<br><br>Agradecemos mucho su atención sobre éste asunto.<br><br>Atentamente,<br><br>"
        . "<br><br></p>";

        $body .="<table border='0'>
           <tr>
              <td><img src='https://drive.google.com/open?id=0B6xKdrrzCRtkRTVkWGZBMzFNRDA' width='80' height='80'></td>
              <td align='left'>
                 <a href='mailto:atencionalcliente@nh.com.ve?Subject=Re:Notificación de Vencimiento de Documentos' target='_blank'>atencionalcliente@nh.com.ve</a> / (0269) 2451291 Ext 710<br><br>
                 <b>NH Import Export, c.a.</b><br>Tel. +58 269.2451291 / <b>0-501-NHADUANA </b>Calle Páez Torre NH Piso 1 y 4. Punto Fijo Edo. Falcón - Venezuela<br>
                 <a href='http://www.nh.com.ve'>www.nh.com.ve</a>
              </td>
           </tr>
           <tr>
              <td colspan='2' align='left'><font color='#a1b4c0'><img src='imagenes/iso9001.png'>Linea de Servicio Certificada Agenciamiento Naviero, Agenciamiento Aduanal y Almacén de Carga bajo Certificación ISO 9001</font></td>
           </tr>
           <tr>
               <td>&nbsp;</td>
           </tr>
           <tr>
              <td colspan='2' align='left'><img src='imagenes/hoja.gif'><font color='#a1b4c0'>No me imprimas si no es necesario. Protejamos el medio ambiente</font></td>
           </tr>
           <tr>
               <td>&nbsp;</td>
           </tr>
           <tr>
              <td colspan='2' align='left' style='font-size: 10px !important;'>
                 *****USO DEL CORREO ELECTRONICO DE NH IMPORT EXPORT ***** Este mensaje puede contener información de interés solo para NH IMPORT<br>
                 EXPORT, C.A. o sus relacionados de negocio. Sólo está permitida su copia, distribución o uso a personas autorizadas. Si Ud. recibió esta nota por<br>
                 error, por favor destrúyalo inmediatamente y notifique al remitente y/o a NH IMPORT EXPORT.</td>
           </tr>
        </table>";
    }

    $mail = new PHPMailer();
    $mail->IsSMTP();
    $mail->SMTPDebug  = 2;

    // dirección remitente, p. ej.: no-responder@miempresa.com
    $mail->From = "winder2006@gmail.com";

    // nombre remitente, p. ej.: "Servicio de envío automático"
    $mail->FromName = "NH Agentes Aduanales y Navieros";

    // podemos hacer varios AddAdress
    $mail->AddAddress("winder2006@gmail.com", "Winder Nanez");
    //$mail->AddReplyTo("winder2006@gmail.com", "Winder Nanez");

    // asunto y cuerpo alternativo del mensaje
    $mail->AltBody = "Cuerpo alternativo para cuando el visor no puede leer HTML en el cuerpo";

    // si el cuerpo del mensaje es HTML
    $mail->MsgHTML($body);

	$mail->Subject = 'Notificación de Vencimiento de Documentos --- '.$cli["codigo"].' - '.$cli["nombre"];
	if(!$mail->Send()) {
		echo ": Error enviando: " . $mail->ErrorInfo."\n";
	} else {
           $hoy = date('d/m/Y');
           // Enviar correo al analista
           $mail2 = new PHPMailer();
           $mail2->IsSMTP();
           $mail2->AddAddress("winder2006@gmail.com", "Winder Nanez");
           $body = "Se informa que el día <b>".$hoy."</b> fue notificada al cliente: <b>".$cli["codigo"].' - '.$cli["nombre"];
           $body .= "</b>; sobre los documentos que se encuentran próximos a vencerse.<br>Si recibe un correo con la información";
           $body .= " solicitada; por favor actualizar la base de datos; de lo contrario haga contacto con el cliente.<br><br>";
           $body .= "<b>PERSONA CONTACTO: ".$cli["contacto"]."</b><br>";
           $body .= "<b>CORREO ELECTRÓNICO: ".$cli["correo"]."</b><br><br>";
           $body .= "Agradecemos mucho su atención sobre este asunto.<br>Atentamente,<br>";
           $body .="<table border='0'>
            <tr>
               <td><img src='https://drive.google.com/open?id=0B6xKdrrzCRtkRTVkWGZBMzFNRDA' width='80' height='80'></td>
               <td align='left'>
                  <a href='mailto:atencionalcliente@nh.com.ve?Subject=Re:Notificación de Vencimiento de Documentos' target='_blank'>atencionalcliente@nh.com.ve</a> / (0269) 2451291 Ext 710<br><br>
                  <b>NH Import Export, c.a.</b><br>Tel. +58 269.2451291 / <b>0-501-NHADUANA </b>Calle Páez Torre NH Piso 1 y 4. Punto Fijo Edo. Falcón - Venezuela<br>
                  <a href='http://www.nh.com.ve'>www.nh.com.ve</a>
               </td>
            </tr>
            <tr>
               <td colspan='2' align='left'><font color='#a1b4c0'><img src='imagenes/iso9001.png'>Linea de Servicio Certificada Agenciamiento Naviero, Agenciamiento Aduanal y Almacén de Carga bajo Certificación ISO 9001</font></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
               <td colspan='2' align='left'><img src='imagenes/hoja.gif'><font color='#a1b4c0'>No me imprimas si no es necesario. Protejamos el medio ambiente</font></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
               <td colspan='2' align='left' style='font-size: 10px !important;'>
                  *****USO DEL CORREO ELECTRONICO DE NH IMPORT EXPORT ***** Este mensaje puede contener información de interés solo para NH IMPORT<br>
                  EXPORT, C.A. o sus relacionados de negocio. Sólo está permitida su copia, distribución o uso a personas autorizadas. Si Ud. recibió esta nota por<br>
                  error, por favor destrúyalo inmediatamente y notifique al remitente y/o a NH IMPORT EXPORT.</td>
            </tr>
         </table>";
           $mail2->MsgHTML($body);
           $mail2->Subject = 'Notificación de Vencimiento de Documentos --- '.$cli["codigo"].' - '.$cli["nombre"];
           if($mail2->send())
           { ?>
		<!--<script>
            alert('Notificación enviada satisfactoriamente');
            $("#pantallas").load("index.php");
           </script>--><?php }



           ?>
		<script>
            alert('Notificación enviada satisfactoriamente');
            $("#pantallas").load("index.php");
        </script><?php
	}
}
else
{ ?>
    <script>
        alert('Ocurrió un error al enviar la notificación (67)');
        $("#pantallas").load("index.php");
    </script><?php
}
//require_once("sfPHPMailer.class.php");

//$mail->SMTPDebug  = 2;
//}
?>
