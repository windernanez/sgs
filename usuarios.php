<div class="cajas">
<div class="titulos_cajas">Registrar Usuarios</div>
<p><label for="nombre">Nombre:</label><input type="text" id="nombre" placeholder="Nombre Completo" autofocus></p>
<p><label for="email">Correo Electrónico:</label><input type="email" id="email" placeholder="Correo Electrónico"></p>
<p><label for="clave">Clave:</label><input type="password" id="clave"></p>
<p><label for="nivel">Nivel:</label>
	<select id="nivel">
		<option value="AD">Coordinador</option>
		<option value="AN">Analistas de Servicios Navieros</option>
		<option value="AO">Analista Administrativo de Operaciones</option>
		<option value="OP">Operador Portuario</option>
		<option value="CO">Consultas</option>
		<option value="FA">Facturación</option>
	</select>
</p>
<p><label>Código:</label><input type="text" id="codigo" placeholder="Código del Usuario" > 
</p>
<p style="text-align:center"><input type="button" value="Registrar" id="registrar_usuario"></p>
</div>

<p id="pantallas_ver"></p>
<script>
	$( "#registrar_usuario" ).click(function() {
	  		usuarios_registrar();
		});
//	$("#el_codigo").load("codigo_generar.php");
	$("#pantallas_ver").load("usuarios_ver.php");
</script>