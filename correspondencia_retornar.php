<?php
	include("includes/conexion.php");	
	include("seguridad.php");
?>
<link rel="stylesheet" type="text/css" href="jsha/shadowbox.css">
	<script type="text/javascript" src="jsha/shadowbox.js"></script>
	<script type="text/javascript">
	  Shadowbox.init();
	</script>

<?php	
		$usuario = $_SESSION['nhsgcusuario'];
		$idusuario = $_SESSION['nhsgcidusuario'];
?>

<div class="cajas">
<div class="titulos_cajas">Retorno de Solicitudes</div>
<p><label>Fecha:</label><span class="cajitas"><?php echo date("d/m/Y");?></span></p>
<p><label for="controlr">Nº de Control:</label><input type="text" id="controlr" placeholder="Nº de Control" autofocus></p>
<p><label>Asunto de Comunicación: </label><span class="cajitas" id="asunto">-</span></p>

<p><label for="obs">N° Tramitación:</label><input type="text" id="naduana" placeholder="Si Aplica" disabled="disabled" onblur="chequear_naduana();"><span id="chequear"></span><input type="hidden" id="esaduana" value="0"> </p>

<p><label for="obs">Observaciones:</label><input type="text" id="obs" placeholder="Observaciones (Si Aplica)"></p>
<p><label>Registro de retorno:</label><span class="cajitas"><?php echo $usuario;?></span><input type="hidden" id="idusuario" value="<?php echo $idusuario;?>"></p>

<p><label for="ladjunto">Copia Digitalizada:</label> <a href="correspondencia_retornar_adjunto.php" rel="shadowbox;width=450;height=100" id="ladjunto">Adjuntar Copia</a> | <span id="veradjunto">Sin Copia</span>
<input type="hidden" id="adjunto" value="">

<p><label for="ladjuntomail">Copia Email:</label> <a href="correspondencia_retornar_adjuntomail.php" rel="shadowbox;width=450;height=100" id="ladjuntomail">Adjuntar Email</a> | <span id="veradjuntomail">Sin Email</span>
<input type="hidden" id="adjuntomail" value="">
</p>

<p><label for="ladjuntoextra">Archivo:</label> <a href="correspondencia_retornar_adjuntoextra.php" rel="shadowbox;width=450;height=100" id="ladjuntoextra">Adjuntar Archivo</a> | <span id="veradjuntoextra">Sin Archivo</span>
<input type="hidden" id="adjuntoextra" value="">
</p>

<p style="text-align:center"><input type="button" value="Registrar Retorno" id="registrar_co_re"></p>
</div>
<p id="pantallas_ver"></p>
<script>

function chequear_naduana()
{
	$("#registrar_co_re").attr("disabled", "disabled");
	naduana = $("#naduana").val();
	if(naduana)
	{		
		$("#chequear").load("correspondencia_chequear.php",{naduana: naduana});		
	}
	else
		alert("N° de Tramitación es obligatorio");
}

	$( "#registrar_co_re" ).click(function() {
			
			if($("#esaduana").val() > 0)	  		
	  		{
	  			if(!$("#naduana").val())
	  			{
	  				alert("El Campo Num de Tramitación es Obligatorio");
	  				$("#naduana").focus();
	  			}
	  			else
	  			 correspondencia_retornar();
	  		}
	  		else
	  			correspondencia_retornar();
		});	
	//$("#pantallas_ver").load("correspondencia_ver.php");
</script>
<div class="clr"></div>
<script type="text/javascript">
	$( "#controlr" ).keypress(function( event ) {
	  if ( event.which == 13 ) 
	  {
	     var control = $('#controlr').val();
		if(control)
			$("#procesar").load("control_validar_retorno.php", {control: control});
	  }
	});  
</script>
