<?php
	include("includes/conexion.php");	
	include("seguridad.php")
?>
<div style="width:960px; margin:0 auto">
	<p style="border: 1px solid grey; padding: 5px"><label for="desde">Desde: </label> <input type="date" name="desde" id="desde" value="<?php echo date("Y-m-d");?>" style="width:140px"> <label for="hasta">Hasta: </label> <input type="date" name="hasta" id="hasta" value="<?php echo date("Y-m-d");?>" style="width:140px">
	
		<label for="linea_re">LI/PRO: </label> 
		<select id="linea_re">
			<option val="todos">Todos</option>
		<?php
			$li = $mysqli->query("SELECT * FROM lineas ORDER BY nombrel ASC");
			if(!$li->num_rows)
				echo '
					<script>
						alert("Debe registrar Lineas/Proyectos para continuar")
						$("#pantallas").load("lineas_proyectos.php");
					</script>
			';


			while($l = $li->fetch_assoc())
			{
				echo '<option value="'.$l['id_linea'].'">'.$l['nombrel'].'</option>';
			}
		?>
	</select>
	
	<label for="embarcacion_re">Emb: </label> 

	<select id="embarcacion_re">
		<option val="todos">Todos</option>
		<?php
			$em = $mysqli->query("SELECT * FROM embarcaciones ORDER BY nombre ASC");
			if(!$em->num_rows)
				echo '
					<script>
						alert("Debe registrar Embarcaciones para continuar")
						$("#pantallas").load("embarcaciones.php");
					</script>
			';
			while($e = $em->fetch_assoc())
			{
				echo '<option value="'.$e['id_embarcacion'].'">'.$e['nombre'].'</option>';
			}
		?>
	</select>

	</p>


<p style="border: 1px solid grey; padding: 5px">
	
		<label for="asunto">Asuntos: </label> 

		<select id="asunto" style="max-width:280px">
		<option val="todos">Todos</option>
		<?php
			$em = $mysqli->query("SELECT * FROM asuntos ORDER BY asunto ASC");
			if(!$em->num_rows)
				echo '
					<script>
						alert("Debe registrar Asuntos de Comunicación")
						$("#pantallas").load("asuntos.php");
					</script>
			';
			while($e = $em->fetch_assoc())
			{
				echo '<option value="'.$e['asunto'].'">'.$e['asunto'].'</option>';
			}
		?>
		</select>


		<label for="autoridad">Autoridad: </label> 
		<select id="autoridad" style="max-width:280px">
			<option val="todos">Todas</option>
		<?php
			$li = $mysqli->query("SELECT * FROM autoridades ORDER BY nombre ASC");
			if(!$li->num_rows)
				echo '
					<script>
						alert("Debe registrar Autoridades para continuar")
						$("#pantallas").load("autoridades.php");
					</script>
			';


			while($l = $li->fetch_assoc())
			{
				echo '<option>'.$l['nombre'].'</option>';
			}
		?>
	</select>

	<label for="xcontrol">Control: </label> 
	<input type="text" id="xcontrol" placeholder="Ej: 111, 222, 333">
	</p>

</div>

<div id="reporte_ver" style="text-align:center">


</div>
<script type="text/javascript">
	reportes_ver();

	$( "#xcontrol" ).keypress(function( event ) {
	  if ( event.which == 13 ) 
	  {
	     reportes_ver();
	  }
	});  
</script>