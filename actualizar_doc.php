<script type="text/javascript" src="datepicker/js/jquery.datepick-es.js"></script>
<?php
include("includes/conexion.php");
include("seguridad.php");
?>
<link rel="stylesheet" type="text/css" href="jsha/shadowbox.css">
<script type="text/javascript" src="jsha/shadowbox.js"></script>
<script type="text/javascript">
//  Shadowbox.init();
</script>

<?php
$usuario = $_SESSION['nhsgcusuario'];
$idusuario = $_SESSION['nhsgcidusuario'];
$doc = $mysqli->query("SELECT * FROM docs_cliente where id=".$_POST['iddoc']." LIMIT 1");
$d = $doc->fetch_assoc();
date_default_timezone_set('America/Caracas');

if($doc->num_rows)
{
    $cli = $mysqli->query("SELECT * FROM clientes where id_cliente=".$d["clientes_id"]." LIMIT 1");
    $c = $cli->fetch_assoc();
    $doccli = $mysqli->query("SELECT * FROM documentos WHERE id_doc=".$d["documentos_id"]." LIMIT 1");
    $d2 = $doccli->fetch_assoc();
}
else
{  ?>
    <script>
        alert('Registro no encontrado');
        var url = "index.php";
        $(location).attr('href',url);
    </script><?php

}
?>

<div class="cajas">
   <div class="titulos_cajas">Documentación del Cliente</div>
   <p><label for="codcli">Cod. Profit:</label><input type="text" id="codcli" disabled="disabled" value="<?php echo $c["codigo"] ?>"></p>
   <p><label>RIF: </label><span class="cajitas" id="rif" disabled="disabled"><?php echo $c["rif"] ?></span></p>
   <p><label>Nombre: </label><span class="cajitas" id="nombre" disabled="disabled"><?php echo $c["nombre"] ?></span></p>
   <div class="titulos_cajas">Control de Vigencia y Adjuntos</div>
        <p>
            <label><b><?php echo 'F. Venc. '.$d2['tipo_doc'] ?>:</b></label><br>
            <input type="text" maxlength="0" id="fvenc_<?php echo strtolower($d2['abr_doc']) ?>" style="width: 150px;">
            <a href="adjuntar_doc_cliente.php?codigo=<?php echo $c['codigo'] ?>&abrdoc=<?php echo strtolower($d2["abr_doc"]) ?>&editar=1" rel="shadowbox;width=450;height=100" id="<?php echo 'link_'.strtolower($d2['abr_doc']) ?>">Adjuntar Archivo</a> |
            <span id="ver_<?php echo strtolower($d2['abr_doc']) ?>">Sin Archivo</span>
            <input type="hidden" id="valoradj_<?php echo strtolower($d2['abr_doc']) ?>" value="">
            <?php
         /*if($l['fvenc']): ?>
         <?php
         endif; ?>

         */ ?>
      </p><hr class="cajas"><?php
?>
<p style="text-align:center"><input type="button" value="Guardar" id="guardar_docs_cli"></p>
</div>
<p id="pantallas_ver"></p>
<div class="clr"></div>
<script type="text/javascript">
    $("[id^=fvenc_]").each(function(){
            $("#"+this.id).datepick({dateFormat: 'yyyy-mm-dd'});
         });
    Shadowbox.init();


    $( "#codcli" ).blur(function() {
         var codcli = $('#codcli').val();

         if(codcli)
            $("#procesar").load("buscar_cliente.php", {codcli: codcli});

         $("[id^=link]").each(function(){
            var codigo = $("#codcli").val();
            var abrdoc = (this.id).substr(5);
//            var valor  = $("#"+abrdoc).val();

            $(this).attr("href", "adjuntar_doc_cliente.php?codigo=" + codigo + "&abrdoc=" + abrdoc);
            $(this).show();
         });
         Shadowbox.init();

         $("[id^=fvenc_]").each(function(){
            $("#"+this.id).datepick({dateFormat: 'yyyy-mm-dd'});
         });
	});

   $( "#guardar_docs_cli" ).click(function() {
       var cancela = 0;
       $("[id^=fvenc_]").each(function(){
           if($("#"+this.id).val() === '')
           {
               alert("Debe indicar la fecha de vencimiento del documento");
               $("#"+this.id).focus();
               cancela = 1;
               return false;
           }
         });

         $("[id^=valoradj_]").each(function(){
           if($("#"+this.id).val() === '')
           {
               alert("Debe adjuntar el archivo");
               cancela = 1;
               return false;
           }
         });

        if(cancela === 1)
            return false;
      procesar_docs_cli(1);
   });
</script>
<script>
$(function() {
//	$('#popupDatepicker').datepick({dateFormat: 'yyyy-mm-dd'});
//	$('#inlineDatepicker').datepick({onSelect: showDate});
});
</script>