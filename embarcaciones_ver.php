<?php
	include("includes/conexion.php");	
?>
<div class="cajas">
	<div class="titulos_cajas">Embarcaciones Registradas</div>

					
		<?php
		$datos = $mysqli->query("SELECT * FROM embarcaciones WHERE(activo = 1) ORDER BY nombre ASC");	

		if($datos->num_rows > 0)
		{

			echo '<table style="width:100%">
					<tr>
						<td style="width:68%">
						  <strong>Nombre de la Embarcación</strong>
						</td>
						<td style="width:30%">
						  <strong>Acciones</strong>
						</td>						
					</tr>	
			';
				$datos->data_seek(0);
				while ($fila = $datos->fetch_assoc()) 
				{					
							$estatus = '<img src="iconos/asemed_eliminar.png" title="Eliminar '.$fila["nombre"].'">';				


					echo '<tr class="filas">';
				    echo '<td>'. $fila['nombre'].'</td>';							    
				    echo '<td class="acciones"><p><a href="javascript:embarcacion_borrar('.$fila['id_embarcacion'].');">'.$estatus.'</a></p></td>';
				    echo '</tr>';
				}
			echo '</table>';
		}	
		else
			echo "No hay Embarcaciones Registradas";

		?>	

	</div>
</div>
