<?php
	include("includes/conexion.php");	
?>
<div class="cajas">
	<div class="titulos_cajas">Configuración de comisiones Registrada</div>

					
		<?php
		$datos = $mysqli->query("SELECT * FROM comisiones ORDER BY autoridad ASC");	

		if($datos->num_rows > 0)
		{

			echo '<table style="width:100%">
					<tr>
						<td style="width:550px">
						  <strong>Autoridad [Asunto]</strong>
						</td>
						<td style="width:50px">
						  <strong>Acciones</strong>
						</td>						
					</tr>	
			';
				$datos->data_seek(0);
				while ($fila = $datos->fetch_assoc()) 
				{					
					$estatus = '<img src="iconos/asemed_eliminar.png" title="Eliminar '.$fila["asunto"].'">';				

					echo '<tr class="filas">';
				    echo '<td><strong>'.$fila['autoridad'].'</strong> ['.$fila['asunto'].']</td>';							    
				    echo '<td class="acciones"><p><a href="javascript:comisiones_borrar('.$fila['id_comision'].');">'.$estatus.'</a></p></td>';
				    echo '</tr>';
				}
			echo '</table>';
		}	
		else
			echo "No hay Configuración Registrada";

		?>	

	</div>
</div>